﻿create schema jonft;

create table jonft.dealerships (
  dealership_id serial primary key,
  dealership_name citext);

 insert into jonft.dealerships (dealership_name)
 values 
 ('Rydell Automotive Group'),
 ('Rydell Auto Center'),
 ('Honda Nissan Grand Forks'),
 ('Apple Valley Ford'); 

 create table jonft.users (
   user_id serial primary key,
   dealership_id integer references jonft.dealerships,
   user_name citext);

insert into jonft.users (dealership_id, user_name)
values 
(1, 'Scott Pearson'),
(1, 'Brett Hanson'),
(1, 'Craig Croaker'),
(1, 'Steve Flaat'),
(2, 'Mike Lear'),
(2, 'John Olderback'),
(3, 'Ben Dover'),
(3, 'Mike Hunt'),
(3, 'Brian McCulley');


select *
from jonft.dealerships a
left join jonft.users b on a.dealership_id = b.dealership_id   

output should look something like 1, Rydell Auto Center, (1,Scott Pearson;2,Brett Hanson;3,Craig Croaker)


CREATE table jonft.test_1 (
  dealership_id integer,
  dealership_name citext, 
  users json)
  
select * from jonft.test_1

select row_to_json(t)
from (
select a.dealership_id, a.dealership_name, b.user_id,
  b.user_name
from jonft.dealerships a
left join jonft.users b on a.dealership_id = b.dealership_id) t

select array_to_json(array_agg(row_to_json(t)))
from (
select a.dealership_id, a.dealership_name, b.user_id,
  b.user_name
from jonft.dealerships a
left join jonft.users b on a.dealership_id = b.dealership_id) t

--http://stackoverflow.com/questions/21137237/postgres-nested-json-array-using-row-to-json

select row_to_json (t)
from (
  select dealership_id, dealership_name,
    (
      select array_to_json(array_agg(row_to_json(jd)))
      from (
        select user_id, user_name
        from jonft.users
        where j.dealership_id = dealership_id
      ) jd
    ) as user_detail
  from jonft.dealerships j
) as t  


select row_to_json (t)
from (
  select *
  from jonft.dealerships) t

select array_agg(row_to_json (t))
from (
  select *
  from jonft.dealerships) t 

select array_to_json(array_agg(row_to_json (t)))
from (
  select *
  from jonft.dealerships) t 

select array_agg(dealership_id::text ||':'||dealership_name)
from jonft.dealerships 

select array_agg(t)
from (
  select *
  from jonft.dealerships) t 

select array_agg(t order by dealership_name)
from (
  select *
  from jonft.dealerships) t   

select array_to_string(array_agg(t), '|')
from (
  select *
  from jonft.dealerships) t 


select unnest(array_agg(t))
from (
  select *
  from jonft.dealerships) t




select array_to_json(array_agg(row_to_json (t)))
from (
  select *
  from jonft.dealerships) t 


select *
from scpp.sales_consultant_configuration








  