﻿CREATE TABLE todayFactRepairOrder ( 
      StoreCode text,
      RO text,
      Line Integer,
      Tag text,
      OpenDateKey Integer,
      CloseDateKey Integer,
      FinalCloseDateKey Integer,
      LineDateKey Integer,
      ServiceWriterKey Integer,
      CustomerKey Integer,
      VehicleKey Integer,
      ServiceTypeKey Integer,
      PaymentTypeKey Integer,
      CCCKey Integer,
      RoCommentKey Integer,
      OpCodeKey Integer,
      CorCodeGroupKey Integer,
      TechGroupKey Integer,
      RoStatusKey Integer,
      StatusKey Integer,
      RoLaborSales Money,
      RoPartsSales Money,
      FlagHours Numeric,
      RoFlagHours Numeric,
      Miles Integer,
      RoCreatedTS TimeStamp,
      LaborSales Money,
      PartsSales Money,
      Sublet Money,
      RoShopSupplies Money,
      RoDiscount Money,
      RoHazardousMaterials Money,
      RoPaintMaterials Money)
WITH (
  OIDS=FALSE
);
ALTER TABLE dimday
  OWNER TO postgres;