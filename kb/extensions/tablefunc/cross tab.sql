﻿-- installed tablefunc extension
-- and while this works it sux, have to hard code column names
-- and don't know how to do multiple value columns (metric_value & metric_qualified)
SELECT *
FROM   crosstab(
      'SELECT employee_number, metric, metric_value
       FROM   scpp.sales_consultant_metric_data
       ORDER  BY 1,2')  -- needs to be "ORDER BY 1,2" here
AS ct (employee_number citext, "Auto Alert Calls per Month" numeric(6,2), 
  "CSI" numeric(6,2), "Email Capture" numeric(6,2), 
  "Logged Opportunity Minimum" numeric(6,2)); 