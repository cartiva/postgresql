6/16/14
/*
struggling with having installed an extension in one schema, not
having access to the functions from another schema,
an answer/workaround (http://stackoverflow.com/questions/12986368/installing-postgresql-extension-to-all-schemas)
*/
-- create the extension against schema pg_catalog;
drop extension "uuid-ossp";
create extension "uuid-ossp" schema pg_catalog;
-- calling the functions now requires no schema qualifier, because
-- pg_catalog is invisibly included in all search paths
select uuid_generate_v4();

12/31/15 Looks like this might not be the best idea
http://dba.stackexchange.com/questions/107389/is-it-recommended-to-install-extensions-into-pg-catalog-schema

Don't install extensions to pg_catalog, because you don't mess with system 
catalog, ever. @Chris demonstrates one reason why. There are others.

However, the "public" schema is in no way special. It's just the default schema 
that's pre-installed in standard distributions so we can get started right away. 
Some DB admins don't use the "public" schema at all, some even delete it.


as a result i have recreated the public schema