﻿--https://www.depesz.com/2010/11/21/waiting-for-9-1-format/

CREATE function get_max( in_table_name TEXT, in_column_name TEXT ) RETURNS TEXT as $$
DECLARE
    reply TEXT;
BEGIN
    EXECUTE 'SELECT max(' || quote_ident( in_column_name ) || ') FROM ' || quote_ident( in_table_name ) INTO reply;
    RETURN reply;
END;
$$ language plpgsql;



do
$$
 declare 
   in_table_name TEXT := 'fin.dim_account';
   in_column_name TEXT := 'account';
   reply TEXT;
begin
    EXECUTE 'SELECT max(' || quote_ident( in_column_name ) || ') FROM ' || quote_ident( in_table_name ) INTO reply;
    raise notice 'reply';
end
$$;

select max(account) from fin.dim_Account

do
$$
 declare 
   in_table_name TEXT := 'fin.dim_account';
   in_column_name TEXT := 'account';
   reply TEXT;
begin
    EXECUTE format( 'SELECT max(%I) FROM %I', in_column_name, in_table_name) INTO reply;
    raise notice 'reply';
end
$$;
