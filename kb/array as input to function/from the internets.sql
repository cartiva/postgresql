﻿-- http://www.postgresql.org/message-id/6d8daee30607181033g1c56b0b9x818087f6d5d4005c@mail.gmail.com
CREATE TABLE jon.ids
(
        id      INTEGER
        , PRIMARY KEY (id)
);

INSERT INTO jon.ids VALUES (1);
INSERT INTO jon.ids VALUES (2);
INSERT INTO jon.ids VALUES (3);

CREATE OR REPLACE FUNCTION jon.example_array_input(INT[]) RETURNS SETOF jon.ids AS
$BODY$
DECLARE
        in_clause ALIAS FOR $1;
        clause  TEXT;
        rec     RECORD;
BEGIN
        FOR rec IN SELECT id FROM jon.ids WHERE id = ANY(in_clause)
        LOOP
                RETURN NEXT rec;
        END LOOP;
        -- final return
        RETURN;
END
$BODY$ language plpgsql;

SELECT * FROM jon.example_array_input('{1,2,4,5,6}'::INT[]);


