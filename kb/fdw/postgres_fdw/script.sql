﻿create extension postgres_fdw schema public

--drop server foreign_server cascade;
create server foreign_server
  foreign data wrapper postgres_fdw
  options (host '172.17.196.73', port '5432', dbname 'Cartiva');

-- shit, import schema only available in 9.5
-- import foreign schema test limit to (ext_bopmast_0201)
-- from foreign_server
-- into scpp

-- this didn't work, after successfully creating mapping could not select from table
-- create user mapping for rydell server foreign_server options(user 'rydell', password 'cartiva');

drop user mapping for rydell server foreign_server;

create user mapping for CURRENT_USER server foreign_server options (password 'cartiva');

DROP FOREIGN TABLE if exists scpp.ext_bopmast_0201 cascade;
-- create in schema public, that way accessible to all schemas without qualification
create foreign table foreign_table (
  bopmast_company_number citext NOT NULL,
  record_key integer NOT NULL,
  record_status citext,
  record_type citext,
  vehicle_type citext,
  franchise_code citext,
  sale_type citext,
  bopmast_stock_number citext,
  bopmast_vin citext,
  buyer_number integer,
  co_buyer_number integer,
  bopmast_search_name citext,
  retail_price numeric(9,2),
  primary_salespers citext,
  secondary_slspers2 citext,
  date_approved date,
  date_capped date
)
server foreign_server
options (schema_name 'test', table_name 'ext_bopmast_0201');


select *
from foreign_table
where date_capped between '2016-01-01' and '2016-01-31'


DROP FOREIGN TABLE if exists scpp.ext_bopmast_0228 cascade;
-- create in schema public, that way accessible to all schemas without qualification
create foreign table ext_bopmast_0228 (
  bopmast_company_number citext NOT NULL,
  record_key integer NOT NULL,
  record_status citext,
  record_type citext,
  vehicle_type citext,
  franchise_code citext,
  sale_type citext,
  bopmast_stock_number citext,
  bopmast_vin citext,
  buyer_number integer,
  co_buyer_number integer,
  bopmast_search_name citext,
  retail_price numeric(9,2),
  primary_salespers citext,
  secondary_slspers2 citext,
  date_approved date,
  date_capped date
)
server foreign_server
options (schema_name 'test', table_name 'ext_bopmast_0228');

select *
from ext_bopmast_0228


