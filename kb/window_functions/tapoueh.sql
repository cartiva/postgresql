﻿-- http://tapoueh.org/blog/2013/08/20-Window-Functions

select *
from generate_series(1,3) as t(x)

1
2
3

select array_agg(x) 
from generate_series(1,3) as t(x)

'{1,2,3}'

select x, array_agg(x) over (order by x)
from generate_series(1,3) as t(x)

1;'{1}'
2;'{1,2}'
3;'{1,2,3}'


select x,
         array_agg(x) over (order by x
                            rows between unbounded preceding
                                     and current row)
    from generate_series(1, 3) as t(x);

1;'{1}'
2;'{1,2}'
3;'{1,2,3}'


select x,
         array_agg(x) over (rows between current row
                                     and unbounded following)
    from generate_series(1, 3) as t(x);    

1;'{1,2,3}'
2;'{2,3}'
3;'{3}'
    