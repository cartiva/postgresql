difference in timestamps in seconds

epoch
For date and timestamp values, the number of seconds since 1970-01-01 00:00:00-00 (can be negative); for interval values, the total number of seconds in the interval

select a.*, now(), 
  extract(epoch from (validthruts - now()))::integer as seconds
from rv.sessions a
where validthruts > now()
limit 100
