﻿

drop table jon.trans1;
drop table jon.trans2;
drop table jon.trans3;


create table if not exists jon.trans1 (
  field1 text not null,
  field2 integer);

create table if not exists jon.trans2 (
  field1 text not null,
  field2 integer);

create table if not exists jon.trans3 (
  field1 text not null, 
  field2 integer);   

-- all one transsaction
-- not possible to nest transactions
-- if one fails, they all fail
-- this is, i think, appropriate for functions that just do multiple
--    inserts/deletes/updates

-- the downside is, not knowing which statement failed,
-- unable to generate user friendly error messages
create or replace function jon.multipleSqlInserts ()
returns void as
$$
insert into jon.trans1 (field1,field2) values ('asdf',1);
insert into jon.trans2 (field1, field2) values ('sdfg', 2);
insert into jon.trans3 (field2) values (3);
$$
language SQL;

select * from jon.multipleSqlInserts();