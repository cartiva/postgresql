﻿RAISE is only available in PLPGSQL

CREATE OR REPLACE FUNCTION jon.raise_test () 
RETURNS integer 
AS $$
  DECLARE
    -- Declare an integer variable for testing.
    an_integer INTEGER = 1;
  
  BEGIN
     -- Raise a debug level message.
    RAISE DEBUG 'The raise_test() function began.';
    
    an_integer = an_integer + 1;
     
     -- Raise a notice stating that the an_integer variable was changed,
     -- then raise another notice stating its new value.
    RAISE NOTICE 'Variable an_integer was changed.';
    RAISE NOTICE 'Variable an_integer''s value is now %.',an_integer;
     
     -- Raise an exception.
    RAISE EXCEPTION 'Variable % changed.  Transaction aborted.',an_integer;
    
    RETURN 1;
  END;
$$ 
LANGUAGE PLPGSQL;

select jon.raise_test ()