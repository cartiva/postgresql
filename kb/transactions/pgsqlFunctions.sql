﻿drop table jon.trans1;
drop table jon.trans2;
drop table jon.trans3;


create table if not exists jon.trans1 (
  field1 text not null,
  field2 integer);

create table if not exists jon.trans2 (
  field1 text not null,
  field2 integer);

create table if not exists jon.trans3 (
  field1 text not null, 
  field2 integer);   

/* SQL
-- all one transsaction
-- not possible to nest transactions
-- if one fails, they all fail
-- this is, i think, appropriate for functions that just do multiple
--    inserts/deletes/updates

-- the downside is, not knowing which statement failed,
-- unable to generate user friendly error messages
*/
i had hoped that this would insert rows into trans1 & trans2 but not trans3
instead the entire thing is rolled back

 aha or oh shit, change raise to raise notice/log/info/warning no message, but the first 2 rows get inserted
  notice, debug, info, log, 
  
create or replace function jon.multiplePgSqlInserts ()
returns void as
$$
begin
  insert into jon.trans1 (field1,field2) values ('asdf',1);
  insert into jon.trans2 (field1, field2) values ('sdfg', 2);
  begin
    insert into jon.trans3 (field1, field2) values (3);
   EXCEPTION when others then
      RAISE exception 'trans3 failed';    
  end;
end;    
$$
language PLPGSQL;

select * from jon.multiplePgSqlInserts();

fuck me, due to my monstrous lack of understanding, must be very clear about
what is expected for each function, and check it

40.2
It is important not to confuse the use of BEGIN/END for grouping statements in PL/pgSQL with the
similarly-named SQL commands for transaction control. PL/pgSQL’s BEGIN/END are only for grouping;
they do not start or end a transaction. Functions and trigger procedures are always executed within a
transaction established by an outer query—they cannot start or commit that transaction, since there would
be no context for them to execute in. However, a block containing an EXCEPTION clause effectively forms
a subtransaction that can be rolled back without affecting the outer transaction. For more about that see
Section 40.6.6.