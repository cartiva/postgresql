﻿/*
docs: 
  Appendix F. Additional Supplied Modules: uuidossp

  http://www.ossp.org/pkg/lib/uuid/  

*/  
CREATE EXTENSION "uuid-ossp" SCHEMA netpromoter

adds adds 10 functions to the schema

select netpromoter.uuid_generate_v1()

select netpromoter.uuid_generate_v4()

6/16/14
/*
struggling with having installed an extension in one schema, not
having access to the functions from another schema,
an answer/workaround (http://stackoverflow.com/questions/12986368/installing-postgresql-extension-to-all-schemas)
*/
-- create the extension against schema pg_catalog;
drop extension "uuid-ossp";
create extension "uuid-ossp" schema pg_catalog;
-- calling the functions now requires no schema qualifier, because
-- pg_catalog is invisibly included in all search paths
select uuid_generate_v4();