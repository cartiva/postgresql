﻿From A Tour of PostgreSQL Data Types
by Jonathan S. Katz
   Jim Mlodgenski
PGCon 2013


Numeric & NYC Sales Tax
 
select 100 * (0.08875)::numeric;  -- 8.875

select 100 * (0.08875)::numeric(7,2) -- 9.0; (.08875 exceeds precision @ casting, rounds to 0.09)

select (100 * 0.08875)::numeric(7,2) -- 8.88 (8.875 exceeds precision @ casting, rounds to 8.88)


Storage: determined by the size of numeric type, no padding

select pg_column_size('123'::numeric(7,2));  -- 8

select pg_column_size('123.45'::numeric(7,2));  -- 10