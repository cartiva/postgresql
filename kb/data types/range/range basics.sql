﻿/*
jonathan katz video presentation
https://www.youtube.com/watch?v=XIcOf7r0dG4
*/

The built-in range types int4range, int8range, and daterange all use a canonical form that includes
the lower bound and excludes the upper bound; that is, [). User-defined range types can use other conventions,
however.

number 1 mindfuck has been the display, it is a mindfuck because one must read the
display COMPLETELY, not just the values, but the representation of the upper and
lower bounds as well, the display in both psql and pgadmin is always [)
[, ] = inclusive
(, ) = exclusive
the generated range is not the values entered, but the range representation for the 
  values and bounds entered

BE DELIBERATE AND CHECK YOUR ASSUMPTIONS UNTIL THIS BECOMES SECOND NATURE  
  
-- examples from manual
 -- includes 3, does not include 7, and does include all points in between
 -- display: [3,7)
SELECT '[3,7)'::int4range;

-- does not include either 3 or 7, but includes all points in between
-- display: '[4,7)'
SELECT '(3,7)'::int4range;

-- includes only the single point 4
-- display: '[4,5)'
SELECT '[4,4]'::int4range;

-- includes no points (and will be normalized to 'empty')
-- display 'empty'
SELECT '[4,4)'::int4range;



SELECT '[2012-03-28, 2012-04-02)'::daterange; 
select daterange('2012-03-28', '2012-04-02','[]')
select lower_inc(daterange('2012-03-28', '2012-04-02','[]'))
select upper_inc(daterange('2012-03-28', '2012-04-02','[]'))  


SELECT int4range(10, 20) * int4range(15, 25);
