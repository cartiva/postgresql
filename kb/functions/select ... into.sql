﻿Stored-procedure approach (PLpgSQL): 
create function withdraw(acctNum text, amount integer) 
returns text as 
$$ 
declare bal integer; 
begin 
/*
whats interesting about this is we have declared a variable: bal
but instead of
bal = (select ....)
select into the variable
select balance into bal 
*/
  select balance into bal 
  from Accounts 
  where acctNo = acctNum; 
  if (bal < amount) then 
    return 'Insufficient Funds'; 
  else 
  update Accounts 
  set balance = balance - amount 
  where acctNo = acctNum; 
  select balance into bal 
  from Accounts 
  where acctNo = acctNum; 
  return 'New Balance: ' || bal; 
  end if; 
end; 
$$ language plpgsql;
