-- returns all column_name data in a single row,in an array
select array_agg(column_name order by ordinal_position)
from test.arkona_syscolumns
where lower(table_name) = 'pyhshdta'
 
-- convert that array into a comma separated string
select array_to_string(array_agg(column_name order by column_name desc), ',')
from test.arkona_syscolumns
where lower(table_name) = 'pyhshdta'