﻿drop table jon.oneTextField cascade;

create table if not exists jon.oneTextField(field1 text);

SQL
-- must use $1, $2, ... $n to reference input parameters;
DROP FUNCTION jon.insertSQL(field1 text);
CREATE OR REPLACE FUNCTION jon.insertSQL(field1 text)
RETURNS VOID 
AS $$
INSERT INTO jon.oneTextField (field1)
  values($1);
$$
LANGUAGE SQL;

-- either of these calls works
select * from jon.insertSQL ('sql1');
select jon.insertSQL ('sql2');



PLPGSQL
-- can reference input parameters by name
-- includes begin ... end
drop function jon.insertPLPGSQL(field1 text);
CREATE OR REPLACE FUNCTION jon.insertPLPGSQL(field1 text)
RETURNS VOID
AS $$
BEGIN
INSERT INTO jon.oneTextField (field1)
  values(field1);
END;
$$
LANGUAGE PLPGSQL;

-- either of these calls works
select jon.insertPLPGSQL('plpgsql1');
select * from jon.insertPLPGSQL('plpgsql2');
