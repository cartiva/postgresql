﻿/*

-- can NOT DO these  within the TRANSACTION, RI locks empAppauthorization,
-- so DELETE can NOT happen until after COMMIT of change on tpEmployees

-- 2.
--SELECT * FROM tpEmployees WHERE username LIKE 'bdale%'  
--select * FROM employeeAppAuthorization WHERE username LIKE 'bdale%'
-- SELECT * FROM tpemployees WHERE username LIKE 'bdal%'

UPDATE tpEmployees
SET username = 'bdalen@rydellcars.com'
WHERE username = 'bdalen@rydellchev.com';

DELETE FROM employeeAppAuthorization
WHERE username = 'bdalen@rydellcars.com';


--2. al berry already has tpEmployee access FROM his days AS a writer, with a username of
--  aberry@rydellchev, change his username, remove ALL the service stuff AND ADD ptomanager
--  SELECT * FROM employeeappauthorization WHERE username = 'aberry@rydellchev.com'
UPDATE tpEmployees
SET username = 'aberry@rydellcars.com'
WHERE username = 'aberry@rydellchev.com';
DELETE FROM employeeAppAuthorization
WHERE username = 'aberry@rydellcars.com'
  AND appcode = 'sco';

BEGIN TRANSACTION;
*/


test the notion of deferrable constraints
and
call a function in check constraint -- category ids in chr.vin_pattern
and maybe non unique constraint names (is it table vs column?)

/*

test the notion of deferrable constraints
by default, declaring a foreign key constraint on a column generates a constraint
where Match type: SIMPLE
      On update: NO ACTION
      On delete: NO ACTION

a foreign key constraint does not generate an index on the referencing column  

match type ([ MATCH FULL | MATCH PARTIAL | MATCH SIMPLE ]): 
  a bit confusing, avoid the whole mess by making referencing columns not null

SET CONSTRAINTS:

Upon creation, a constraint is given one of three characteristics: 
DEFERRABLE INITIALLY DEFERRED, DEFERRABLE INITIALLY IMMEDIATE, or NOT DEFERRABLE. 
The third class is always IMMEDIATE and is not affected by the SET CONSTRAINTS command. 
The first two classes start every transaction in the indicated mode, 
but their behavior can be changed within a transaction by SET CONSTRAINTS.

Currently, only UNIQUE, PRIMARY KEY, REFERENCES (foreign key), and EXCLUDE constraints 
are affected by this setting. NOT NULL and CHECK constraints are always checked 
immediately when a row is inserted or modified (not at the end of the statement). 
Uniqueness and exclusion constraints that have not been declared DEFERRABLE 
are also checked immediately.


Leaning toward declaring REFERENCE constraints as DEFERRABLE INITIALLY IMMEDATE, then
using SET CONSTRAINTS when i need DEFERRABLE for a specific transaction
   
t1               t2
-----            --------         
PK field1 -----o<
field2
field3



*/



no action allows the check to be deferred until later in the transacion, restrict does not