﻿/*
shit that i learned
constraint names need to be unique within a schema
  could not make the primary key constraint in multiple tables be pk
  i now don't know if this is true or not: eg: category_id_chk defined and
    used in multiple tables ?!?

no good way of dealing with nulls vs emplty strings in csv files (or foreign tables)
  instead of not null, use CHECK (field_name <> '')  

hmmm, why fuck around with fdw at all, just copy from file into chr.tables  
doing an insert into select from is not very snappy: 3 mil ros for style_generic_equipment
  took 10 minutes
  ads took 25 minutes
  copy with all constraints in place took almost 10 fucking minutes
  copy with no constraints: 22 seconds
  copy with no constraints, then apply constraints: ~ 1 minute
and copy is all about fast xfr between db and files
*/

--drop schema if exists chr cascade;
--create schema if not exists chr;

drop table if exists chr.version;
drop table if exists chr.vin_equipment;
drop table if exists chr.style_generic_equipment;
drop table if exists chr.vin_pattern_style_mapping;
drop table if exists chr.style_wheel_base;
drop table if exists chr.vin_pattern;
drop table if exists chr.category;
drop table if exists chr.year_make_model_style;

create Table if not exists chr.year_make_model_style (
  chrome_style_id bigint constraint year_make_model_style_pk primary key,
  country text check (country <> ''),
  year text check (year <> ''),
  division_name TEXT check (division_name <> ''),
  subdivision_name TEXT check (subdivision_name <> ''),
  model_name TEXT check (model_name <> ''),
  style_name TEXT check (style_name <> ''),
  trim_name text,
  mfr_style_code TEXT,
  fleet_only TEXT check (fleet_only <> ''),
  available_in_nvd TEXT check (available_in_nvd <> ''),
  division_id bigint not null,
  subdivision_id bigint not null,
  model_id bigint not null,
  auth_builder_style_id TEXT check (auth_builder_style_id <> ''),
  historical_style_id TEXT);
copy chr.year_make_model_style from 'c:\chrome\YearMakeModelStyle.txt' with (format 'csv', quote '~', null 'NULL', header 'TRUE');

create Table if not exists chr.category (
  category_id text constraint category_pk primary key,
  description TEXT check (description <> ''),
  category_utf text,
  category_type text);
copy chr.category from 'c:\chrome\Category.txt' with (format 'csv', quote '~', null 'NULL', header 'TRUE');

/*
ok, this is where it is going to get dicey, all the categoryIDs that are optional
in the csv file they are empty strings
in this table, bigint will not take an empty string, but will be doing
which is why, in ads, i just made all the id collumns character rather than number
ok, here is my decision: category.categoryid becomes a text field.
*/

create Table if not exists chr.vin_pattern (
  vin_pattern_id bigint constraint vin_pattern_pk primary key,
  vin_pattern text check (vin_pattern <> ''),
  country text check (country <> ''),
  year text check (year <> ''),
  vin_division_name text check(vin_division_name <> ''),
  vin_model_name text check(vin_model_name <> ''),
  vin_style_name text,
  engine_type_category_id text,
  engine_size text,
  engine_cid bigint,
  fuel_type_category_id text,
  forced_induction_category_id text,
  transmission_type_category_id text,
  manual_trans_avail text,
  auto_trans_avail text,
  gvwr_range text);
copy chr.vin_pattern from 'c:\chrome\VINPattern.txt' with (format 'csv', quote '~', null 'NULL', header 'TRUE');

create Table if not exists chr.style_wheel_base (
  chrome_style_id bigint,
  wheel_base numeric(12,2));
copy chr.style_wheel_base FROM 'c:\chrome\StyleWheelBase.txt' WITH (format 'csv', quote '~', null 'NULL', header 'TRUE');
alter table chr.style_wheel_base
alter column chrome_style_id set not null,
alter column wheel_base set not null,
add primary key (chrome_style_id,wheel_base),
add constraint chrome_style_id_fk foreign key (chrome_style_id) references chr.year_make_model_style(chrome_style_id);

create Table if not exists chr.vin_pattern_style_mapping (
  vin_mapping_id bigint, -- primary key,
  chrome_style_id bigint, -- references chr.year_make_model_style,
  vin_pattern_id bigint); -- references chr.vin_pattern_id);
copy chr.vin_pattern_style_mapping from 'c:\chrome\VINPatternStyleMapping.txt' with ( format 'csv', quote '~', null 'NULL', header 'TRUE');
alter table chr.vin_pattern_style_mapping
add primary key (vin_mapping_id),
add constraint chrome_style_id_fk foreign key(chrome_style_id) references chr.year_make_model_style(chrome_style_id),
add constraint vin_pattern_fk foreign key(vin_pattern_id) references chr.vin_pattern(vin_pattern_id);

create Table if not exists chr.style_generic_equipment (
  chrome_style_id bigint,
  category_id text,
  style_availability text);
copy chr.style_generic_equipment from 'c:\chrome\StyleGenericEquipment.txt' with (format 'csv', quote '~', null 'NULL', header 'TRUE');
-- now add the constraints:
alter table chr.style_generic_equipment
alter column chrome_style_id set not null,
alter column category_id set not null,
add constraint chrome_style_id_fk foreign key (chrome_style_id) references chr.year_make_model_style (chrome_style_id),
add constraint category_id_fk foreign key (category_id) references chr.category (category_id),
add constraint style_availability_chk check (style_availability <> ''),
add constraint category_id_chk check (category_id <> ''),
add primary key (chrome_style_id,category_id);

create table if not exists chr.vin_equipment(
  vin_pattern_id bigint,
  category_id text,
  vin_availability text);
copy chr.vin_equipment from 'c:\chrome\VinEquipment.txt' with (format 'csv', quote '~', null 'NULL', header 'TRUE');  
alter table chr.vin_equipment
alter column vin_pattern_id set not null,
add constraint vin_availability_chk check(vin_availability <> ''),
add constraint category_id_chk check (category_id <> ''),
add constraint vin_pattern_id_fk foreign key (vin_pattern_id) references chr.vin_pattern (vin_pattern_id),
add constraint category_id_fk foreign key (category_id) references chr.category (category_id),
add primary key (category_id,vin_pattern_id);
 
create table if not exists chr.version(
  product text not null,
  data_version timestamp not null,
  data_release bigint not null,
  schema_name text not null,
  schema_version text not null,
  country text not null,
  language text not null);
copy chr.version from 'c:\chrome\Version.txt' with (format 'csv', quote '~', null 'NULL', header 'TRUE');  

