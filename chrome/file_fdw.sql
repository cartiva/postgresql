﻿/*
Server is the source of data
each foreign server uses one FDW, one FDW supports multiple servers

this is what i am thinking:
a separate schema for the files (chf)
a separate schema for the db (chr) that will include RI, PK, FK, constraints ...
each schema will have a table obj category, vin_pattern, ...

nightly: ftp new files, check version, if new, 
  a transaction that zaps the chr tables
  and repopulates them with data from chf.tables

drop schema ch cascade;
create schema chf;

******** this whole approach is being ditched, faster to copy files ********
******** directly to the db, then implse constraints                ********

*/

drop server if exists chrome_files cascade;
create server chrome_files foreign data wrapper file_fdw;

drop foreign table if exists chf.category;
create foreign Table if not exists chf.category (
  category_id bigint,
  description text,
  category_utf text,
  category_type text)
server chrome_files
options (filename 'c:\chrome\category.txt', format 'csv', quote '~', null 'NULL', header 'TRUE');

drop foreign table if exists chf.style_generic_equipment;
create foreign Table if not exists chf.style_generic_equipment (
  chrome_style_id bigint,
  category_id bigint,
  style_availability text)
server chrome_files
options (filename 'c:\chrome\StyleGenericEquipment.txt', format 'csv', quote '~', null 'NULL', header 'TRUE');

drop foreign table if exists chf.style_wheel_base;
create foreign Table if not exists chf.style_wheel_base (
  chrome_style_id bigint,
  sheel_base numeric(12,2))
server chrome_files
options (filename 'c:\chrome\StyleWheelBase.txt', format 'csv', quote '~', null 'NULL', header 'TRUE');

drop foreign table if exists chf.data_version;
create foreign Table if not exists chf.data_version (
  product text,
  data_version timestamp,
  data_release_id bigint,
  schema_name text,
  schema_version text,
  country text,
  language text)
server chrome_files
options (filename 'c:\chrome\version.txt', format 'csv', quote '~', null 'NULL', header 'TRUE');

drop foreign table if exists chf.vin_equipment;
create foreign Table if not exists chf.vin_equipment (
  vin_pattern_id bigint,
  category_id bigint,
  vin_availability text)
server chrome_files
options (filename 'c:\chrome\VINEquipment.txt', format 'csv', quote '~', null 'NULL', header 'TRUE');

/*
VINPatter: the categoryid columns are not truely foreign keys as
they are often an empty string, so, instead of bigint,
make them text, at least here
*/
drop foreign table if exists chf.vin_pattern;
create foreign Table if not exists chf.vin_pattern (
  vin_pattern_id bigint,
  vin_pattern text,
  country text,
  year text,
  vin_division_name text,
  vin_model_name text,
  vin_style_name text,
  engine_type_category_id bigint,
  engine_size text,
  engine_cid bigint,
  fuel_type_category_id text,
  forced_induction_category_id text,
  transmission_type_category_id text,
  manual_trans_avail text,
  auto_trans_avail text,
  gvwr_range text)
server chrome_files
options (filename 'c:\chrome\VINPattern.txt', format 'csv', quote '~', null 'NULL', header 'TRUE');

drop foreign table if exists chf.vin_pattern_style_mapping;
create foreign Table if not exists chf.vin_pattern_style_mapping (
  vin_mapping_id bigint,
  chrome_style_id bigint,
  vin_pattern bigint)
server chrome_files
options (filename 'c:\chrome\VINPatternStyleMapping.txt', format 'csv', quote '~', null 'NULL', header 'TRUE');

drop foreign table if exists chf.year_make_model_style;
create foreign Table if not exists chf.year_make_model_style (
  chrome_style_id bigint,
  country text,
  year text,
  division_name text,
  subdivision_name text,
  model_name text,
  style_name text,
  trim_name text,
  mfr_style_code text,
  fleet_only text,
  available_in_nvd text,
  division_id bigint,
  subdivision_id bigint,
  model_id bigint,
  auth_builder_style_id text,
  historical_style_id text)
server chrome_files
options (filename 'c:\chrome\YearMakeModelStyle.txt', format 'csv', quote '~', null 'NULL', header 'TRUE');
