﻿-- -- this does not work
-- declare @val;
-- set @val = "open"; 
-- SET @data = select *
-- from scpp.months
-- where open_closed = @val;
-- PRINT @data;


-- -- this works
-- SET @data = select *
-- from scpp.months
-- where open_closed = 'open';
-- PRINT @data;


-- this works
-- creates tables public.table0 thru public.table19
DECLARE @I, @T; -- Variable names begin with a @
SET @I = 0; -- @I is an integer
WHILE @I < 20
BEGIN
    SET @T = 'table' + CAST (@I AS STRING); -- Casts @I
    CREATE TABLE @T (id integer primary key, data text);

    SET @I = @I + 1;
END

-- and this drops them
DECLARE @I, @T; -- Variable names begin with a @
SET @I = 0; -- @I is an integer
WHILE @I < 20
BEGIN
    SET @T = 'table' + CAST (@I AS STRING); -- Casts @I
    drop TABLE @T;

    SET @I = @I + 1;
end


-- this works
SET @A = INTEGER(100, 200);
PRINT @A; -- Prints an integer between 100 and 200
PRINT @A; -- Prints another integer between 100 and 200    
END

-- this works with out having to resort to scription
-- http://nixmash.com/postgresql/using-postgresql-anonymous-code-blocks/
-- NOTE the only way to return data from an anonymous block is create a
-- temp table, populate it in the block, query it outside of the block
do
$$
declare 
  url text := 'fuck you what';
  i integer;
  arr int[] := '{43,6,8,10}';
begin
  for i in 1..3
  loop
    raise notice 'Hello World';
  end loop;
  raise notice '%', url;


drop table if exists _x;

create temporary table _x as select generate_subscripts(arr, 1), unnest(arr);  

end
$$;

select * from _x;

-- drop table if exists _x;

-- or use a cursor
do
$$
declare
  _query text;
  _cursor constant refcursor := '_cursor';
begin
  _query := 'select * from scpp.metrics';
  open _cursor for execute _query; 
end
$$;

fetch all from _cursor;  
close _cursor;


-- nested blocks
-- http://www.postgresqltutorial.com/plpgsql-block-structure/
-- PL/pgSQL block syntax
[ <<label>> ]
[ DECLARE
    declarations ]
BEGIN
    statements;
 ...
END [ label ];


DO $$ 
<<outer_block>>
DECLARE
  counter integer := 0;
BEGIN 
   counter := counter + 1;
   RAISE NOTICE 'The current value of counter is %', counter;
 
   DECLARE 
       counter integer := 0;
   BEGIN 
       counter := counter + 10;
       RAISE NOTICE 'The current value of counter in the subblock is %', counter;
       RAISE NOTICE 'The current value of counter in the outer block is %', outer_block.counter;
   END;
 
   RAISE NOTICE 'The current value of counter in the outer block is %', counter;
   
END outer_block $$;