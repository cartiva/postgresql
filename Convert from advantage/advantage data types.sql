      {* data types *}
      const ADS_TYPE_UNKNOWN = 0;
      const ADS_LOGICAL = 1; {* 1 byte logical value *}
      const ADS_NUMERIC = 2; {* DBF character style numeric *}
      {* Date field.  With ADS_NTX, ADS_CDX, and
       * ADS_VFP< this is an 8 byte field of the form
       * CCYYMMDD.  With ADS_ADT, it is a 4 byte
       * Julian date. *}
      const ADS_DATE = 3;
      const ADS_STRING = 4; {* Character data *}
      const ADS_MEMO = 5; {* Variable length character data *}

      {* the following are extended data types *}
      const ADS_BINARY = 6; {* BLOB - any data *}
      const ADS_IMAGE = 7; {* BLOB - bitmap *}
      const ADS_VARCHAR = 8; {* variable length character field *}
      const ADS_COMPACTDATE = 9; {* DBF date represented with 3 bytes *}
      const ADS_DOUBLE = 10; {* IEEE 8 byte floating point *}
      const ADS_INTEGER = 11; {* IEEE 4 byte signed long integer *}

      {* the following are supported with the ADT format *}
      const ADS_SHORTINT = 12; {* IEEE 2 byte signed short integer *}
      {* 4 byte long integer representing
       * milliseconds since midnight *}
      const ADS_TIME = 13;
      {* 8 bytes.  High order 4 bytes are a
       * long integer representing Julian date.
       * Low order 4 bytes are a long integer
       * representing milliseconds since
       * midnight *}
      const ADS_TIMESTAMP = 14;
      const ADS_AUTOINC = 15; {* 4 byte auto-increment value *}
      const ADS_RAW = 16; {* Untranslated data *}
      const ADS_CURDOUBLE = 17; {* IEEE 8 byte floating point currency *}
      const ADS_MONEY = 18; {* 8 byte, 4 implied decimal Currency Field *}
      const ADS_LONGLONG = 19; {* 8 byte integer *}
      const ADS_CISTRING = 20; {* CaSe INSensiTIVE character data *}
      const ADS_ROWVERSION = 21; {* 8 byte integer, incremented for every update, unique to entire table *}
      const ADS_MODTIME = 22; {* 8 byte timestamp, updated when record is updated *}
      const ADS_VARCHAR_FOX = 23; {* Visual FoxPro varchar field *}
      const ADS_VARBINARY_FOX = 24; {* Visual FoxPro varbinary field *}
      const ADS_SYSTEM_FIELD = 25; {* For internal usage *}
      const ADS_NCHAR = 26; {* Unicode Character data *}
      const ADS_NVARCHAR = 27; {* Unpadded Unicode Character data *}
      const ADS_NMEMO = 28; {* Variable Length Unicode Data *}