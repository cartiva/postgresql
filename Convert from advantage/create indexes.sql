/*
exempt indexes LIKE PK
these are included IN the CREATE tables
ads separates multiple columns with a ; postgres with a ,

3/13/15:
*a* : chokes on field names with a #, replace with _ (to match TABLE generation)
*b* : chokes index names with a -, replace with _
*/
DECLARE @str memo;
DECLARE @crlf string;
DECLARE @tableName string;
DECLARE @columnName string; 
DECLARE @lastField integer;
DECLARE @i integer;
DECLARE @schema string;
DECLARE @cur CURSOR AS
  SELECT LEFT(name, 35) AS name, LEFT(parent, 35) AS parent, 
    LEFT(index_expression, 75) AS index_expression,
    index_options
  FROM system.indexes  
  -- SET the TABLE name -------------------------------------------------------
  WHERE parent = 'factorycertifications';
--  WHERE name NOT LIKE '%PK%'
--    AND parent NOT LIKE '%zunused%';
--AND parent IN ('dimServiceWriter','day','factVehicleSale','dimVehicle',
--  'edwEmployeeDim', 'dimSalesPerson','factRepairOrder');	
@crlf = Char(13) + Char(10);  
@str = '';  
-- SET the schema -------------------------------------------------------------
@schema = 'ads';
OPEN @cur;   
TRY
  WHILE FETCH @cur DO
    @str = @str + 'CREATE ' + iif(@cur.index_options = 2051, 'UNIQUE ', '') + ' INDEX '
-- *b*    
--    + trim(lower(@cur.parent)) + '_' + TRIM(lower(@cur.name)) 
    + replace(trim(lower(@cur.parent)) + '_' + TRIM(lower(@cur.name)),'-','_')    
	+ ' ON ' + @schema + '.' + TRIM(lower(@cur.parent)) 
-- *a*  
--    + '(' + replace(TRIM(lower(@cur.index_expression)), ';', ',') + ');' + @crlf;
    + '(' + replace(replace(TRIM(lower(@cur.index_expression)), ';', ','),'#','_') + ');' + @crlf;    
  END WHILE;  
FINALLY
  CLOSE @cur;
END TRY;    

SELECT @str FROM system.iota;
  