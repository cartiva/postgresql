﻿/* 
basic template
returns table
language sql
adds comment
adds row(s) to ''.sys_tables_in_functions
*/
---------------------------------------------------------------------------------------------------

create or replace function ''. ()
returns table ()
as $$
/*

*/
select 

$$
language sql;
comment on function ''.***********() is '';
insert into ''.sys_tables_in_functions values
('','','')

---------------------------------------------------------------------------------------------------

create or replace function rv.pto_get_manager_employees(_employee_number text)
returns table (
  employeenumber text,
  name text,
  department text,
  title text,
  ptoAnnirversary date,
  ptoExpiringWeeks integer,
  ptoHoursEarned integer,
  ptoHoursRequested integer,
  ptoHoursApproved integer,
  ptoHoursUsed numeric(8,2),
  ptoHoursRemaining numeric(8,2)
  )
as $$
/*
select * from rv.pto_get_manager_employees('187675')
*/
select employeenumber, name, department, title, ptoAnniversary,
  ptoExpiringWeeks, ptoHoursEarned, ptoRequested, ptoApproved,
  ptoUsed,
  ptoHoursEarned - ptoUsed - ptoRequested - ptoApproved as ptoHoursRemaining
from (    
  select a.employeenumber, a.name, a.department, a.title,a.ptoAnniversary,
    a.ptoExpiringWeeks, a.ptoHoursEarned + 
      (  
        select coalesce(sum(hours), 0)::integer as ptoAdustments
        from rv.pto_adjustments
        where employeenumber = a.employeenumber
          and fromdate = a.cur_from_date) as ptoHoursEarned,  
    (
      select coalesce(sum(hours), 0)::integer as ptoRequested
      from rv.pto_requests
      where employeenumber = a.employeenumber
        and thedate between current_Date and a.cur_thru_date
        and lower(requestType) = 'paid time off'
        and requestStatus = 'Pending'),
    (
      select coalesce(sum(hours), 0)::integer as ptoApproved
      from rv.pto_requests
      where employeenumber = a.employeenumber
        and thedate between current_date and a.cur_thru_date
        and lower(requestType) = 'paid time off'
        and requestStatus = 'Approved'),
    (
      select coalesce(sum(hours), 0) as ptoUsed
      from rv.pto_used
      where employeenumber = a.employeenumber
      and thedate between a.cur_from_date and a.cur_thru_date)           
       
  from (
    select d.employeenumber, trim(d.firstname) || ' ' || d.lastname as name,
      c.department, c.position as title, e.ptoAnniversary, 
      f.ptoExpiringWeeks, f.ptoHoursEarned, cur_from_date, cur_thru_date
    from rv.ptoAuthorization a
    inner join rv.ptoPositionFulfillment b on a.authByDepartment = b.department
      and a.authByPosition = b.position
    inner join rv.ptoPositionFulfillment c on a.authForDepartment = c.department
      and a.authForPosition = c.position
    inner join dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
      and d.currentrow = true
      and d.active = 'Active'
      and d.fullparttime = 'Full'
    inner join rv.ptoEmployees e on d.employeenumber = e.employeenumber
    left join (-- current pto period for the employee
      select d.employeenumber, d.cur_from_date, upper(d.cur_period) as cur_thru_date, 
        (upper(d.cur_period) - current_date)/7 as ptoExpiringWeeks,
        d.ptohoursearned
      from (
          select c.employeenumber, c.cur_from_date,
            case extract(year from c.cur_from_date) -- generate current period thrudate and subsequent range
              when 2014 then -- use the alloc.fromdate & alloc.thrudate
                daterange(c.cur_from_date, (c.thrudate - interval '1 day')::date, '[]') 
              else 
                case extract(year from c.thrudate)
                  when 9999 then -- add a year to cur_from_date
                    daterange(c.cur_from_date, (c.cur_from_date + interval '1 year' - interval '1 day')::date, '[)')
                  else -- use alloc.
                    daterange(c.cur_from_date, (c.thrudate - interval '1 day')::date, '[]') 
                end
            end as cur_period,       
            alloc_hours as ptoHoursEarned
          from (
            SELECT LEFT(b.username, 25), a.employeenumber, 
              a.fromdate, a.thrudate, 
              case extract(year from a.fromdate)
                when 2014 then a.fromdate
                else 
                  case 
                      when extract(year from current_date) - extract(year from a.fromdate) > 0 then 
                      to_date(trim(to_char(extract(year from current_date)::integer, '9999')) || '-' ||          
                      trim(to_char(extract(month from b.ptoanniversary)::integer,'00')) || '-' ||
                      trim(to_char(extract(day from b.ptoanniversary)::integer,'00')), 'yyyy-mm-dd')    
                    else a.fromdate    
                  end 
              end as cur_from_date, hours::integer as alloc_hours  
            FROM rv.pto_employee_pto_allocation a
            INNER JOIN rv.ptoemployees b ON a.employeenumber = b.employeenumber
            WHERE current_date BETWEEN a.fromdate AND a.thrudate) c) d) f on d.employeenumber = f.employeenumber
    where b.employeenumber = _employee_number) a) x  
order by ptoExpiringWeeks;    

$$
language sql;
comment on function rv.pto_get_manager_employees(text) is 'populate pto manager list of employees w/pto info';
insert into rv.sys_tables_in_functions values
('pto_get_manager_employees','rv','pto_adjustments'),
('pto_get_manager_employees','rv','pto_requests'),
('pto_get_manager_employees','rv','pto_used'),
('pto_get_manager_employees','rv','ptoAuthorization'),
('pto_get_manager_employees','rv','ptoPositionFulfillment'),
('pto_get_manager_employees','dds','edwEmployeeDim'),
('pto_get_manager_employees','rv','ptoemployees'),
('pto_get_manager_employees','rv','pto_employee_pto_allocation')

---------------------------------------------------------------------------------------------------

create or replace function rv.pto_get_employee_pto_used(_employee_number text)
returns table (
      employeeNumber text,
      theDate DATE,
      hours numeric(8,2),
      theType text)
as $$
/*
select * from rv.pto_get_employee_pto_used('187675')
*/
with
  emp_no as ( -- the employeenumber
--     select '1106421'::text as employeenumber),
   select _employee_number as employeenumber),
  currentperiod as ( -- current pto period for employeenumber
    select c.employeenumber, c.username, c.ptoAnniversary, c.cur_from_date,
      case extract(year from c.cur_from_date) -- generate current period thrudate and subsequent range
        when 2014 then -- use the alloc.fromdate & alloc.thrudate
          daterange(c.cur_from_date, (c.thrudate - interval '1 day')::date, '[]') 
        else 
          case extract(year from c.thrudate)
            when 9999 then -- add a year to cur_from_date
              daterange(c.cur_from_date, (c.cur_from_date + interval '1 year' - interval '1 day')::date, '[)')
            else -- use alloc.
              daterange(c.cur_from_date, (c.thrudate - interval '1 day')::date, '[]') 
          end
      end as cur_period,       
      alloc_hours
    from (
      SELECT LEFT(b.username, 25), a.employeenumber, b.ptoAnniversary, b.username,
        a.fromdate, a.thrudate, 
        case extract(year from a.fromdate)
    --     if the alloc.fromdate is not 2014 (initial rollout, extended current periods)
    --       and curdate - alloc.fromdate > 1 year (20 year folks with 1 row in pto_alloc with thrudate = 12/31/9999
    --       then need to calculate the current fromdate
          when 2014 then a.fromdate
          else 
            case 
            -- from date assembled from year of curdate and month/day of anniv date
                when extract(year from current_date) - extract(year from a.fromdate) > 0 then 
                to_date(trim(to_char(extract(year from current_date)::integer, '9999')) || '-' ||          
                trim(to_char(extract(month from b.ptoanniversary)::integer,'00')) || '-' ||
                trim(to_char(extract(day from b.ptoanniversary)::integer,'00')), 'yyyy-mm-dd')    
              else a.fromdate    
            end 
        end as cur_from_date, hours::integer as alloc_hours  
      FROM rv.pto_employee_pto_allocation a
      INNER JOIN rv.ptoemployees b ON a.employeenumber = b.employeenumber
      WHERE current_date BETWEEN a.fromdate AND a.thrudate
        and a.employeenumber = (select * from emp_no)) c)
select a.employeenumber, a.thedate, a.hours, 'Used'
from rv.pto_used a
inner join rv.pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
  and current_date <@ (select cur_period from currentperiod)
where a.employeenumber = (select employeenumber from emp_no)
  and a.thedate <@ (select cur_period from currentperiod)  
union 
select a.employeenumber, a.thedate, a.hours, 'Approved'
from rv.pto_requests a
inner join rv. pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
  and current_date <@ (select cur_period from currentperiod)
where a.employeenumber = (select employeenumber from emp_no)
  and a.thedate <@ (select cur_period from currentperiod) 
  and a.requestType = 'Paid Time Off'
  and a.requestStatus = 'Approved'  
union 
select a.employeenumber, a.thedate, a.hours, 'Requested'
from rv.pto_requests a
inner join rv. pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
  and current_date <@ (select cur_period from currentperiod)
where a.employeenumber = (select employeenumber from emp_no)
  and a.thedate <@ (select cur_period from currentperiod) 
  and a.requestType = 'Paid Time Off'
  and a.requestStatus = 'Pending'   
order by theDate desc ;  
$$
language sql;
comment on function rv.pto_get_employee_pto_used(text) is 'for the current pay period, return used/req/appr pto for the employee page summary';
insert into rv.sys_tables_in_functions values
('pto_get_employee_pto_used','rv','pto_requests'),
('pto_get_employee_pto_used','rv','pto_employee_pto_allocation'),
('pto_get_employee_pto_used','rv','pto_used'),
('pto_get_employee_pto_used','rv','ptoemployees'),
('pto_get_employee_pto_used','',''),
('pto_get_employee_pto_used','',''),
('pto_get_employee_pto_used','',''),
('pto_get_employee_pto_used','',''),
('pto_get_employee_pto_used','','');


---------------------------------------------------------------------------------------------------
create or replace function rv.pto_get_employee_pto(_employee_number citext)
returns table (
  employeeNumber citext,
  employee_name text,
  store text,
  originalHireDate DATE,
  latestHireDate DATE ,
  department text,
  employtee_position text,
  ptoAdministrator text,
  ptoAnniversary DATE,
  ptoHoursEarned integer,
  ptoHoursUsed numeric(8,2),
  ptoHoursRemaining numeric(8,2),
  title text,
  ptoFromDate DATE,
  ptoThruDate DATE,
  ptoPolicyHtml text,
  ptoHoursApproved numeric(8,2),
  ptoHoursRequested numeric(8,2),
  totalDaysInPeriod Integer,
  remainingDaysInPeriod Integer)
as $$
/*
select * from rv.pto_get_employee_pto('152235')
*/
with 
  emp_no as ( -- the employeenumber
--     select '196341'::citext as employeenumber),
   select _employee_number as employeenumber),
  currentperiod as ( -- employeenumber, username, ptoAnniv, current pto period, curr alloc hours
    select c.employeenumber, c.username, c.ptoAnniversary, c.cur_from_date,
      case extract(year from c.cur_from_date) -- generate current period thrudate and subsequent range
        when 2014 then -- use the alloc.fromdate & alloc.thrudate
          daterange(c.cur_from_date, (c.thrudate - interval '1 day')::date, '[]') 
        else 
          case extract(year from c.thrudate)
            when 9999 then -- add a year to cur_from_date
              daterange(c.cur_from_date, (c.cur_from_date + interval '1 year' - interval '1 day')::date, '[)')
            else -- use alloc.
              daterange(c.cur_from_date, (c.thrudate - interval '1 day')::date, '[]') 
          end
      end as cur_period,       
      alloc_hours
    from (
      SELECT LEFT(b.username, 25), a.employeenumber, b.ptoAnniversary, b.username,
        a.fromdate, a.thrudate, 
        case extract(year from a.fromdate)
    --     if the alloc.fromdate is not 2014 (initial rollout, extended current periods)
    --       and curdate - alloc.fromdate > 1 year (20 year folks with 1 row in pto_alloc with thrudate = 12/31/9999
    --       then need to calculate the current fromdate
          when 2014 then a.fromdate
          else 
            case 
            -- from date assembled from year of curdate and month/day of anniv date
                when extract(year from current_date) - extract(year from a.fromdate) > 0 then 
                to_date(trim(to_char(extract(year from current_date)::integer, '9999')) || '-' ||          
                trim(to_char(extract(month from b.ptoanniversary)::integer,'00')) || '-' ||
                trim(to_char(extract(day from b.ptoanniversary)::integer,'00')), 'yyyy-mm-dd')    
              else a.fromdate    
            end 
        end as cur_from_date, hours::integer as alloc_hours  
      FROM rv.pto_employee_pto_allocation a
      INNER JOIN rv.ptoemployees b ON a.employeenumber = b.employeenumber
      WHERE current_date BETWEEN a.fromdate AND a.thrudate
        and a.employeenumber = (select * from emp_no)) c),
  emp_dim as ( -- name, store, full/part, orig hiredate, latest hiredate
    select trim(a.firstname) || ' ' || a.lastname as name, 
      case a.storecode
        when 'RY1' then 'RY1 - GM'
        when 'RY2' then 'RY2 - Honda'
      end as store, a.fullparttime, c.ymhdto, c.ymhdte
    from dds.edwEmployeeDim a
    inner join emp_no b on a.employeenumber = b.employeenumber
    inner join dds.stgArkonaPYMAST c on a.storecode = c.ymco_
      and a.employeenumber = c.ymempn
    where a.currentrow = true),
  emp_pos as (-- department, position, admin name
    select b.department, b.position, trim(e.firstname) || ' ' ||e.lastname as adminName
    from emp_no a
    inner join rv.ptoPositionFulfillment b on a.employeenumber = b.employeenumber    
    inner join rv.ptoAuthorization c on b.department = c.authForDepartment
      and b.position = c.authForPosition
    inner join rv.ptoPositionFulfillment d on c.authByDepartment = d.department
      and c.authByPosition = d.position
    inner join dds.edwEmployeeDim e on d.employeenumber = e.employeenumber
      and e.currentrow = true),
  pto_adj as ( -- pto adjustments for current period
    select coalesce(sum(a.hours)::integer, 0) as ptoAdjustment
    from rv.pto_adjustments a
    where employeenumber = (select employeenumber from emp_no) 
      and a.fromdate = (select lower(cur_period) from currentperiod)),
  pto_used as ( -- ptoUsed
    select coalesce(sum(a.hours)::integer, 0) as ptoUsed
    from rv.pto_used a
    where a.employeenumber = (select employeenumber from emp_no) 
      and a.thedate <@ (select cur_period from currentperiod)),
  pto_app_req as (--ptoApproved
    select 
      coalesce(sum(case when requestStatus::citext = 'approved' then a.hours::integer else 0 end), 0) as ptoApproved,
      coalesce(sum(case when requestStatus::citext = 'pending' then a.hours::integer else 0 end), 0) as ptoRequested
    from rv.pto_requests a
    where requestType::citext = 'paid time off'
      and a.employeenumber = (select employeenumber from emp_no) 
      and a.thedate between current_date and (select upper(cur_period) from currentperiod))
select a.employeenumber, c.name, c.store, c.ymhdto, c.ymhdte, d.department, d.position, 
  d.adminName, b.ptoanniversary, b.alloc_hours + e.ptoAdjustment as ptoHoursEarned,
  f.ptoUsed::numeric(8,2), 
  (b.alloc_hours + e.ptoAdjustment - f.ptoUsed - g.ptoApproved - g.ptoRequested)::numeric(8,2) as ptoHoursRemaining,
  trim(d.department) || ':' || d.position as title,
  lower(b.cur_period) as ptoFrom, upper(b.cur_period) as ptoThru,
  h.ptoPolicyHtml,
  g.ptoApproved::numeric(8,2), g.ptoRequested::numeric(8,2),
  (upper(b.cur_period) - lower(b.cur_period)) + 1 totalDaysInPeriod,
  upper(b.cur_period) - current_date as remainingDaysInPeriod
from emp_no a
inner join currentperiod b on a.employeenumber = b.employeenumber 
cross join emp_dim c  
cross join emp_pos d
cross join pto_adj e
cross join pto_used f
cross join pto_app_req g
left join rv.ptoDepartmentPositions h on d.department = h.department
  and d.position = h.position;
$$
language sql;
comment on function rv.pto_get_employee_pto(citext) is 'populate pto employee page summary';
insert into rv.sys_tables_in_functions values
('pto_get_employee_pto','rv','ptoDepartmentPositions'),  
('pto_get_employee_pto','rv','pto_requests'),  
('pto_get_employee_pto','rv','pto_used'),  
('pto_get_employee_pto','rv','pto_adjustments'),  
('pto_get_employee_pto','dds','edwEmployeeDim'),  
('pto_get_employee_pto','rv','ptoPositionFulfillment'),  
('pto_get_employee_pto','rv','ptoAuthorization'),  
('pto_get_employee_pto','dds','stgArkonaPYMAST'),  
('pto_get_employee_pto','rv','pto_employee_pto_allocation'),  
('pto_get_employee_pto','rv','ptoemployees');

































