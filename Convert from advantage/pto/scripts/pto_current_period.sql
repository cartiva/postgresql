﻿with 
  curdate -- allows testing future dates
    as (
      select current_date as thedate), -- '2016-02-01'::date as thedate),-- allows testing future dates
  emp_no as ( -- the employeenumber
    select '279630'::citext as employeenumber)
select c.employeenumber, c.username, c.ptoAnniversary, c.fromdate, c.thrudate,
  c.cur_from_date,
-- so, i need to case the range generation thrudate
  case extract(year from c.cur_from_date) -- generate current period thrudate and subsequent range
    when 2014 then -- use the alloc.fromdate & alloc.thrudate
      daterange(c.cur_from_date, (c.thrudate - interval '1 day')::date, '[]') 
    else 
      case extract(year from c.thrudate)
        when 9999 then -- add a year to cur_from_date
          daterange(c.cur_from_date, (c.cur_from_date + interval '1 year' - interval '1 day')::date, '[)')
        else -- use alloc.
          daterange(c.cur_from_date, (c.thrudate - interval '1 day')::date, '[]') 
      end
  end as cur_period_for_real,  
  daterange(c.cur_from_date, (c.cur_from_date + interval '1 year' - interval '1 day')::date, '[)') as cur_period,
  alloc_hours
from ( -- generates current period fromdate
  SELECT LEFT(b.username, 25), a.employeenumber, b.ptoAnniversary, b.username,
    a.fromdate, a.thrudate,
    case extract(year from a.fromdate)
--     if the alloc.fromdate is not 2014 (initial rollout, extended current periods)
--       and curdate - alloc.fromdate > 1 year (20 year folks with 1 row in pto_alloc with thrudate = 12/31/9999
--       then need to calculate the current fromdate
      when 2014 then a.fromdate
      else 
        case 
        -- from date assembled from year of curdate and month/day of anniv date
            when extract(year from (select thedate from curdate)) - extract(year from a.fromdate) > 0 then 
            to_date(trim(to_char(extract(year from (select thedate from curdate))::integer, '9999')) || '-' ||          
            trim(to_char(extract(month from b.ptoanniversary)::integer,'00')) || '-' ||
            trim(to_char(extract(day from b.ptoanniversary)::integer,'00')), 'yyyy-mm-dd')    
          else a.fromdate    
        end 
    end as cur_from_date, hours::integer as alloc_hours  
  FROM rv.pto_employee_pto_allocation a
  INNER JOIN rv.ptoemployees b ON a.employeenumber = b.employeenumber
  WHERE (select thedate from curdate) BETWEEN a.fromdate AND a.thrudate
    and a.employeenumber = (select * from emp_no)) c