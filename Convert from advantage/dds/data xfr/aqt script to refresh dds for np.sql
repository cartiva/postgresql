delete from dds.dimsalesperson;
delete from dds.dimServicewriter;
delete from dds.dimVehicle;
delete from dds.edwEmployeeDim;
delete from dds.factRepairOrder;
delete from dds.factVehicleSale;

--aqt load,tab=dds.dimsalesperson,dbs=postgres local,source=table,fromtab=dimsalesperson,fromdbs=dds
--aqt load,tab=dds.dimservicewriter,dbs=postgres local,source=table,fromtab=dimservicewriter,fromdbs=dds
--aqt load,tab=dds.dimvehicle,dbs=postgres local,source=table,fromtab=dimvehicle,fromdbs=dds
--aqt load,tab=dds.edwemployeedim,dbs=postgres local,source=table,fromtab=edwemployeedim,fromdbs=dds
--aqt load,tab=dds.factrepairorder,dbs=postgres local,source=table,fromtab=factrepairorder,fromdbs=dds
--aqt load,tab=dds.factvehiclesale,dbs=postgres local,source=table,fromtab=factvehiclesale,fromdbs=dds
