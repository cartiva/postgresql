/*
select left(name,50) AS name, proc_input, proc_output, sql_script FROM system.ansi_storedprocedures

select left(name,50) AS name, proc_input, proc_output, 
  sql_script 
FROM system.ansi_storedprocedures
WHERE name NOT LIKE 'zunu%'

input/output
the number of params = number of semi colons

@parameterCount = (
  SELECT length(@search) - length(replace(@search, ' ', '')) + 1
  FROM system.iota);
  
SELECT proc_input, length(proc_input), length(replace(proc_input, ';', ''))

select left(name,50) AS name, 
  proc_input, 
  CASE 
    WHEN proc_input IS NULL THEN 0
	ELSE length(proc_input) - length(replace(proc_input, ';', ''))
  END AS inputParamCount,
  proc_output, 
  CASE 
    WHEN proc_output IS NULL THEN 0
	ELSE length(proc_output) - length(replace(proc_output, ';', ''))
  END AS outputParamCount,  
  sql_script,
  length(sql_script) - length(replace(sql_script, char(13), '')) AS scriptLength
FROM system.ansi_storedprocedures  
--WHERE name = 'GetDepartmentScoresRolling'
WHERE name NOT LIKE 'zunu%'
*/

/* 
may eventually want to translate parameter data types
AND include double/numeric scale/precision
*/
DECLARE @crlf string;
DECLARE @str memo;
DECLARE @i integer;
DECLARE @o integer;
DECLARE @l integer;
DECLARE @inputStr string;
DECLARE @inputName string;
DECLARE @inputType string;
DECLARE @outputStr string;
DECLARE @outputName string;
DECLARE @outputType string;
DECLARE @scriptStr string;
DECLARE @procName string;
DECLARE @schema string;
DECLARE @procCur CURSOR AS 
  SELECT left(name,50) AS name, 
    CASE 
      WHEN proc_input IS NULL THEN 0
  	ELSE length(proc_input) - length(replace(proc_input, ';', ''))
    END AS inputParamCount,
    CASE 
      WHEN proc_output IS NULL THEN 0
  	ELSE length(proc_output) - length(replace(proc_output, ';', ''))
    END AS outputParamCount,  
    length(sql_script) - length(replace(sql_script, char(13), '')) AS scriptLength
  FROM system.ansi_storedprocedures  
--  WHERE name = 'GetDepartmentScoresRolling';
  WHERE name NOT LIKE 'zunu%';
DECLARE @inputCur CURSOR AS 
  SELECT proc_input
  FROM system.ansi_storedprocedures
  WHERE name = @procCur.name;
DECLARE @outputCur CURSOR AS 
  SELECT proc_output
  FROM system.ansi_storedprocedures
  WHERE name = @procCur.name; 
DECLARE @scriptCur CURSOR AS 
  SELECT sql_script
  FROM system.ansi_storedprocedures
  WHERE name = @procCur.name;   
@schema = 'netpromoter'; --------------------------/ SET the schema /-----------
@crlf = Char(13) + Char(10);  
@str = '';  
OPEN @procCur;
TRY
WHILE FETCH @procCur DO
  @procName = TRIM(@procCur.name);
  @i = @procCur.inputParamCount;
  @o = @procCur.outputParamCount;
  @l = @procCur.scriptLength;
  @str = TRIM(@str) + 'CREATE OR REPLACE FUNCTION ' + @schema + '.' + @procName + '()';
  OPEN @inputCur;
  TRY
  WHILE FETCH @inputCur DO
    @inputStr = @inputCur.proc_input;
    @str = @str + @crlf;
    @str = @str + space(4) + ' ---- Input Parameters ----' + @crlf;
    WHILE @i > 0 DO
      @inputName = LEFT(@inputStr, position(',' IN @inputStr) - 1); -- up to first comma
      @inputStr = replace(@inputStr, LEFT(@inputStr, position(',' IN @inputStr)), ''); -- get rid of name
      @inputType = LEFT(@inputStr, position(';' IN @inputStr) - 1); -- up to first semicolon
      IF position (',' IN @inputType) > 0 THEN -- IS there a comma
      @inputType = LEFT(@inputType, position(',' IN @inputType) -1); -- just up to the comma
      ENDIF;
      IF @i = 1 THEN 
        @str = @str + space(4) + @inputName + ' ' + @inputType + @crlf;
      ELSE 
        @str = @str + space(4) + @inputName + ' ' + @inputType  + ',' + @crlf;
      ENDIF;
      @inputStr = right(@inputStr, length(@inputStr) - position(';' IN @inputStr));
      @i = @i - 1;
    END WHILE; -- WHILE @i > 0
  END WHILE; -- WHILE FETCH @inputCur
  FINALLY
    CLOSE @inputCur;
  END TRY;
	OPEN @outputCur; 
	TRY
	WHILE FETCH @outputCur DO
    @outputStr = @outputCur.proc_output;
	  @str = @str + space(4) + ' ---- Output Parameters ----' + @crlf;
		WHILE @o > 0 DO 
      @outputName = LEFT(@outputStr, position(',' IN @outputStr) - 1); -- up to first comma
      @outputStr = replace(@outputStr, LEFT(@outputStr, position(',' IN @outputStr)), ''); -- get rid of name
      @outputType = LEFT(@outputStr, position(';' IN @outputStr) - 1); -- up to first semicolon
      IF position (',' IN @outputType) > 0 THEN -- IS there a comma
        @outputType = LEFT(@outputType, position(',' IN @outputType) -1); -- just up to the comma
      ENDIF;
      IF @o = 1 THEN 
        @str = @str + space(4) + @outputName + ' ' + @outputType + @crlf;
      ELSE 
        @str = @str + space(4) + @outputName + ' ' + @outputType  + ',' + @crlf;
      ENDIF;
      @outputStr = right(@outputStr, length(@outputStr) - position(';' IN @outputStr));
      @o = @o - 1;		
		END WHILE; -- WHILE @o > 0
	END WHILE;  -- WHILE FETCH @outputCur
	FINALLY
	  CLOSE @outputCur;
  END TRY;
	OPEN @scriptCur;
	TRY
	  WHILE FETCH @scriptCur DO
      @scriptStr = @scriptCur.sql_script;
  	  @str = @str + space(4) + ' ---- Sscript ----' + @crlf;	
			@scriptStr = space(8) + @scriptStr;
			@scriptStr = replace(@scriptStr, @crlf, @crlf + space(8));
			@str = @str + @scriptStr;
--  		@str = @str + space(8) + @scriptStr;		
		END WHILE; -- WHILE FETCH @scriptCur
  FINALLY
	  CLOSE @scriptCur;
	END TRY;
END WHILE;
FINALLY
  CLOSE @procCur;
END TRY;    

SELECT @str FROM system.iota;

/*
unsubscribeTS loses datatype AND semicolon
after processing SurveSentTS
replacing ALL occurences of timestamp;
need to substring instead
substring using length doesn't WORK, have trimmed off the numeric part (after comma)
substring 1 -> first semicolon should work
*/