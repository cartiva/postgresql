on local
database: Test1

create new schema np (netpromoter) 

Z:\E\Postgresql\Convert from advantage\create tables.sql
run the resultant script, creating the tables

indexes?
this seems to work well
Z:\E\Postgresql\Convert from advantage\create indexes.sql

CREATE [ UNIQUE ] INDEX [ CONCURRENTLY ] [ name ] ON table_name [ USING method ]
	( { column_name | ( expression ) } [ COLLATE collation ] [ opclass ] 
	[ ASC | DESC ] [ NULLS [ WITH ( storage_parameter = value [, ... ] ) ]
	[ TABLESPACE tablespace_name ]
	[ WHERE predicate ]

now, load data from cartiva.netpromoter

need an aqt connection to local
for the text generator:
--aqt load,tab=np.$2,dbs=postgres local,source=table,fromtab=netpromoter.$2,fromdbs=postgres

ran this from local sql window, slower than shit
2 possibilities:
  1. running against local host
  2. indexes

so, 1, try without indexes, use cstfbNpDataRolling (240 rows)
same thing, slower than shit
run it against postgres (not local)
same thing, somewhat less slow, but still SLOW
so, wtf, go ahead and run it
fuck, this is way too slow, try exporting into insert statements, entire schema in one

export data from netpromoter.* to file, paste this into generate text:

--aqt export,file="C:\Users\jon\Desktop\npData.sql",type=insert,filemode=append,nulls=NULL,prompt=no,insert_tab=np.$2,uniwrite=n
select * from netpromoter.$2

export to file takes about 30 seconds, generates a 19M file (66310 rows)
and running the insert statement takes abt 30 sec

now do the indexes, another 10 sec

now, the heart of the matter
today is monday, what i want to do is to rewrite the stored procs to use
the foreign tables, ie, instead of the overnight script parsing the
downloaded csv files, use them directly

recreate the foreign tables ala Z:\E\Postgresql\kb\fdw\file_fdw\digital air strike.sql
foreign server already exists (exists at cluster/instance level)
foreign tables exist at schema level

6/18/2014
looking at possible bringing relevant dds tables into local, schema dds
tables required for all the netpromoter updating: 
dimServiceWriter, day, factVehicleSale, dimVehicle, edwEmployeeDim, 
dimSalesPerson, factRepairOrder
  shit, 
  dimTech has boolean field (active), aqt exports that (in import statement) as 1/0
	postgres chokes on it
	try the loader instead
aqt generatator statement (load local postgres with dds tables, statement only
  loads tables that exist in postgres)
--aqt load,tab=dds.$2,dbs=postgres local,source=table,fromtab=$2,fromdbs=dds
ok, that worked
indexes from ads

now, the stored procs that have already been written on 172, oh wait, i have those scripts ...

6/23/2014
getting back to this, want to update the localhost data
data in pg.localhost is still 2 weeks old, have the last 2 downloads from digitalairstrike
in E\Postgresql\Convert from advantage\netpromoter\data xfr
should be doable, should probably first update the data in pg.localhost.dds
which will be good enough to do the last 2 weeks of np updates
ok, done (Z:\E\Postgresql\Convert from advantage\dds\data xfr\aqt script to refresh dds for np.sql)

do a dump/backup of localhost db and np before proceeding
navicat generated a plain text filed with a .sql extension, 
first cut of pgadmin generated a .backup file
this will have to be revisited.

