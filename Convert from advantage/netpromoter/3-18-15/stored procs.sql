﻿/* 
basic template
returns table
language sql
adds comment
adds row(s) to np.sys_tables_in_functions
*/
create or replace function np. ()
returns table ()
as $$
/*

*/
select 

$$
language sql;
comment on function np.***********() is '';
insert into np.sys_tables_in_functions values
('','','')

---------------------------------------------------------------------------------------------------

create or replace function np.from_thru_dates ()
returns table (from_date date, thru_date date)
as $$
/*
should this be returning a range of typ daterange?
that would necessitate refactoring everywhere these dates are used
*/
select b.theDate as from_date, c.theDate as thru_date
from (
  select max(theDate) - 89 as fromDate, max(thedate) as thruDate
  from dds.day
  where dayOfWeek = 1
    and theDate <= current_date) a
left join dds.day b on a.fromDate = b.thedate
left join dds.day c on a.thruDate = c.thedate; 
$$
language sql;

comment on function np.from_thru_dates() is '90 day window back from most recent Sunday';

insert into np.sys_tables_in_functions values
('from_thru_dates','dds','day');

select * from np.from_thru_dates ()

-------------------------------------------------------------------------------------------------

create or replace function np.get_market_scores ()
returns table (sent integer, returned integer, response_rate numeric(8,2), score numeric(8,2))
as $$
/*

*/
SELECT sent, returned, responserate, score
FROM np.cstfbNpDataRolling
WHERE market = 'GF'
  and storecode = 'all'
  AND department = 'all'
  AND thrudate = (
    SELECT MAX(thrudate)
    FROM np.cstfbNpDataRolling);

$$
language sql;
comment on function np.get_market_scores() is 'most recent market summary';
insert into np.sys_tables_in_functions values
('get_market_scores','dnp','cstfbNpDataRolling');

select * from np.get_market_scores();

-------------------------------------------------------------------------------------------------

create or replace function np.get_store_scores (_store_code citext)
returns table (sent integer, returned integer, response_rate numeric(8,2), score numeric(8,2))
as $$
/*

*/
SELECT sent, returned, responserate, score
FROM np.cstfbNpDataRolling
WHERE storecode = _store_code
  AND department = 'all'
  AND thrudate = (
    SELECT MAX(thrudate)
    FROM np.cstfbNpDataRolling); 

$$
language sql;
comment on function np.get_store_scores(citext) is 'most recent store summary';
insert into np.sys_tables_in_functions values
('get_store_scores','np','cstfbNpDataRolling');

select * from np.get_store_scores('ry2')

-------------------------------------------------------------------------------------------------

create or replace function np.get_department_scores(_store_code citext, _department citext)
returns table (sent integer, returned integer, response_rate numeric(8,2), score numeric(8,2))
as $$
/*

*/
SELECT sent, returned, responserate, score
FROM np.cstfbNpDataRolling
WHERE storecode = _store_code
  AND 
    CASE _department
      WHEN 'SALES' THEN department = 'Sales' AND subdepartment = 'ALL'
      WHEN 'NEW' THEN subdepartment = 'NEW'
      WHEN 'USED' THEN subdepartment = 'USED'
      WHEN 'SERVICE' THEN department = 'Service' AND subdepartment = 'ALL'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
    END
  AND thrudate = (
    SELECT MAX(thrudate)
    FROM np.cstfbNpDataRolling);    

$$
language sql;
comment on function np.get_department_scores(citext,citext) is 'most recent department summary';
insert into np.sys_tables_in_functions values
('get_department_scores','np','cstfbNpDataRolling');

---------------------------------------------------------------------------------------------------

create or replace function np.get_department_people_scores (_store_code citext, _department citext)
returns table (name citext,sent integer, returned integer, response_rate numeric(8,2), score numeric(8,2))
as $$
/*

*/
declare _from_date date := (
  select max(x.theDate) - 89 
  from dds.day x
  where dayofweek = 1
  and x.thedate <= current_date);
declare _thru_date date := (
  select max(y.theDate) 
  from dds.day y
  where dayofweek = 1
  and y.thedate <= current_date); 
begin 
return query 
	SELECT a.employeeName, COUNT(*)::integer AS sent,
	  SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END)::integer AS returned,    
	  CASE COUNT(*)
	    WHEN 0 THEN 0
	    ELSE 
	      ROUND(100 * (SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) * .01/COUNT(*)), 2)
	  END AS responseRate,
	  ROUND(CASE SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) 
	    WHEN 0 THEN 0
	    ELSE 
	      (SUM(CASE WHEN a.referralLikelihood > 8 THEN 1 ELSE 0 END) * 
		100/SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) 
	      -
	      SUM(CASE WHEN a.referralLikelihood < 7 AND a.customerResponse = true THEN 1 ELSE 0 END) * 100
		/SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END))/100.0
	  END, 2)  AS Score      
	FROM np.cstfbNpData a
	WHERE a.transactionDate BETWEEN _from_date AND _thru_date
	  AND a.storeCode = _store_code
	  AND 
	    CASE _department
      WHEN 'SALES' THEN transactionType = 'Sales'
      WHEN 'NEW' THEN subDepartment = 'NEW'
      WHEN 'USED' THEN subDepartment = 'USED'
      WHEN 'SERVICE' THEN transactionType = 'Service'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
	    END
	GROUP BY a.employeeName
	order by a.employeeName; 
END;  
$$
language plpgsql;
comment on function np.get_department_people_scores(citext,citext) is 'most recent summary for individuals';
insert into np.sys_tables_in_functions values
('get_department_people_scores','dds','day'),
('get_department_people_scores','np','cstfbNpData');

select * from np.get_department_people_scores('rY2','mainSHOP');
---------------------------------------------------------------------------------------------------

create or replace function np.get_market_scores_rolling()
returns table (from_date date, thru_date date, response_rate numeric(8,2), score numeric(8,2))
as $$
/*

*/
select a.fromDate, a.thruDate, a.responseRate, a.score
from np.cstfbNpDataRolling a
where a.storecode = 'All'
  and thruDate in (
    SELECT x.theDate as mondays
    FROM dds.day x
    WHERE x.dayofWeek = 2
      AND x.theDate <= current_date
    ORDER BY x.theDate DESC
    LIMIT 12)
order by a.thruDate;    
$$
language sql;
comment on function np.get_market_scores_rolling() is 'response rate and scores for last 12 90 day windows by week';
insert into np.sys_tables_in_functions values
('get_market_scores_rolling','np','cstfbNpDataRolling'),
('get_market_scores_rolling','dds','day');

select * from np.get_market_scores_rolling();
---------------------------------------------------------------------------------------------------

create or replace function np.get_store_scores_rolling (_store_code citext)
returns table (from_date date, thru_date date, response_rate numeric(8,2), score numeric(8,2))
as $$
/*

*/
select a.fromDate, a.thruDate, a.responseRate, a.score
from np.cstfbNpDataRolling a
where a.storecode = _store_code
  AND a.department = 'All'
  and thruDate in (
    SELECT x.theDate as mondays
    FROM dds.day x
    WHERE x.dayofWeek = 2
      AND x.theDate <= current_date
    ORDER BY x.theDate DESC
    LIMIT 12)
order by a.thruDate;   

$$
language sql;
comment on function np.get_store_scores_rolling(citext) is 'response rate and scores for last 12 90 day windows by week';
insert into np.sys_tables_in_functions values
('get_store_scores_rolling','np','cstfbNpDataRolling'),
('get_store_scores_rolling','dds','day');

select * from np.get_store_scores_rolling('ry1');
---------------------------------------------------------------------------------------------------

create or replace function np.get_department_scores_rolling (_store_code citext, _department citext)
returns table (from_date date, thru_date date, response_rate numeric(8,2), score numeric(8,2))
as $$
/*

*/
select a.fromDate, a.thruDate, a.responseRate, a.score
from np.cstfbNpDataRolling a
where a.storecode = 'ry1' --_store_code
  AND a.department = 'All'
  and thruDate in (
    SELECT x.theDate as mondays
    FROM dds.day x
    WHERE x.dayofWeek = 2
      AND x.theDate <= current_date
    ORDER BY x.theDate DESC
    LIMIT 12)
  AND 
    CASE _department
      WHEN 'SALES' THEN department = 'Sales' AND subdepartment = 'ALL'
      WHEN 'NEW' THEN subdepartment = 'NEW'
      WHEN 'USED' THEN subdepartment = 'USED'
      WHEN 'SERVICE' THEN department = 'Service' AND subdepartment = 'ALL'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
    END
order by a.thruDate;   
$$
language sql;
comment on function np.get_department_scores_rolling(citext, citext) is 'response rate and scores for last 12 90 day windows by week';
insert into np.sys_tables_in_functions values
('get_department_scores_rolling','np','cstfbNpDataRolling'),
('get_department_scores_rolling','dds','day');

select * from np.get_department_scores_rolling('ry1','SALES');
---------------------------------------------------------------------------------------------------

create or replace function np.get_market_surveys()
returns table (employee citext, the_date date, ro_stock citext, customer_name citext, referral_likelihood integer, customer_experience citext)
as $$
/*

*/
with
  date_range as (
    select daterange(a.from_date, b.thru_date, '[]') as _range
    from (
      select max(thedate) - 89 as from_date
      from dds.day
      where dayofweek = 1
        and thedate <= current_date) a
    cross join (
      select max(thedate) as thru_date
      from dds.day
      where dayofweek = 1
        and thedate <= current_date) b)
select a.employeename, a.transactiondate, a.transactionNumber, a.customerName,
  a.referralLikelihood, a.customerExperience
  from np.cstfbNpData a
   where transactiondate <@ (select _range from date_range) 
    and customerResponse = true
order by a.transactionDate desc;
$$
language sql;
comment on function np.get_market_surveys() is 'all surveys for current 90 day window';
insert into np.sys_tables_in_functions values
('get_market_surveys','dds','day'),
('get_market_surveys','np','cstfbNpData');

select * from np.get_market_surveys();

---------------------------------------------------------------------------------------------------

create or replace function np.get_store_surveys(_store_code citext)
returns table (employee citext, the_date date, ro_stock citext, customer_name citext, referral_likelihood integer, customer_experience citext)
as $$
/*

*/
with
  date_range as (
    select daterange(a.from_date, b.thru_date, '[]') as _range
    from (
      select max(thedate) - 89 as from_date
      from dds.day
      where dayofweek = 1
        and thedate <= current_date) a
    cross join (
      select max(thedate) as thru_date
      from dds.day
      where dayofweek = 1
        and thedate <= current_date) b)
select a.employeename, a.transactiondate, a.transactionNumber, a.customerName,
  a.referralLikelihood, a.customerExperience
  from np.cstfbNpData a
   where transactiondate <@ (select _range from date_range) 
    and customerResponse = true
    and a.storecode = _store_code
order by a.transactionDate desc; 

$$
language sql;
comment on function np.get_store_surveys(citext) is 'all surveys for current 90 day window';
insert into np.sys_tables_in_functions values
('get_store_surveys','dds','day'),
('get_store_surveys','np','cstfbNpData');

select count(*) from (
select * from np.get_store_surveys('ry2')) a

---------------------------------------------------------------------------------------------------

create or replace function np.get_department_surveys(_store_code citext, _department citext)
returns table (employee citext, the_date date, ro_stock citext, customer_name citext, referral_likelihood integer, customer_experience citext)
as $$
/*

*/
with
  date_range as (
    select daterange(a.from_date, b.thru_date, '[]') as _range
    from (
      select max(thedate) - 89 as from_date
      from dds.day
      where dayofweek = 1
        and thedate <= current_date) a
    cross join (
      select max(thedate) as thru_date
      from dds.day
      where dayofweek = 1
        and thedate <= current_date) b)
select a.employeename, a.transactiondate, a.transactionNumber, a.customerName,
  a.referralLikelihood, a.customerExperience
  from np.cstfbNpData a
   where transactiondate <@ (select _range from date_range) 
    and customerResponse = true
    and a.storecode = _store_code
  AND 
    CASE _department
      WHEN 'SALES' THEN transactionType = 'Sales'
      WHEN 'NEW' THEN subDepartment = 'NEW'
      WHEN 'USED' THEN subDepartment = 'USED'
      WHEN 'SERVICE' THEN transactionType = 'Service'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
    END
order by a.transactionDate desc; 

$$
language sql;
comment on function np.get_department_surveys(citext,citext) is 'all surveys for current 90 day window';
insert into np.sys_tables_in_functions values
('get_department_surveys','dds','day'),
('get_department_surveys','np','cstfbNpData');

select count(*) from (
select * from np.get_department_surveys('ry1','detail')) a;

---------------------------------------------------------------------------------------------------

create or replace function np.get_employee_surveys(_employee citext)
returns table (employee citext, the_date date, ro_stock citext, customer_name citext, referral_likelihood integer, customer_experience citext)
as $$
/*

*/
with
  date_range as (
    select daterange(a.from_date, b.thru_date, '[]') as _range
    from (
      select max(thedate) - 89 as from_date
      from dds.day
      where dayofweek = 1
        and thedate <= current_date) a
    cross join (
      select max(thedate) as thru_date
      from dds.day
      where dayofweek = 1
        and thedate <= current_date) b)
select a.employeename, a.transactiondate, a.transactionNumber, a.customerName,
  a.referralLikelihood, a.customerExperience
  from np.cstfbNpData a
   where transactiondate <@ (select _range from date_range) 
    and customerResponse = true
    and a.employeename = _employee
order by a.transactionDate desc; 

$$
language sql;
comment on function np.get_employee_surveys(citext) is 'all surveys for current 90 day window';
insert into np.sys_tables_in_functions values
('get_employee_surveys','dds','day'),
('get_employee_surveys','np','cstfbNpData');

select count(*) from (
select * from np.get_employee_surveys('leslie champagne')) a;

---------------------------------------------------------------------------------------------------