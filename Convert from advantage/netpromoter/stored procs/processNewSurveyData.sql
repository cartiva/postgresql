﻿/*
6/11, close, just not quite sure
*/

-- drop function np.processNewSurveyData();
create or replace function np.processNewSurveyData()
returns bigint
as $$
/*
-- dummy up stgDigitalAirStrike with data from xfmDigitalAirStrike
-- which will allow me to configure the remaining stored procs.
-- 1. populate stgDigitalAirStrike with June 2014 data
delete from np.stgDigitalAirStrike;
insert into np.stgDigitalAirStrike (SurveyID, DealerName, VIN, 
          RONumber, SurveySentTS, TransactionType, CustomerName, 
          CustomerEmail, UnsubscribeTS, SurveyResponseTS, 
          ConsentToPublish, StarRating, ReferralLikelihood, 
          CustomerExperience, CustomerSuggestion, SalesPersonName)
select np.uuid_generate_v4(), DealerName, VIN, 
          RONumber, SurveySentTS, TransactionType, CustomerName, 
          CustomerEmail, UnsubscribeTS, SurveyResponseTS, 
          ConsentToPublish, StarRating, ReferralLikelihood, 
          CustomerExperience, CustomerSuggestion, SalesPersonName 
from np.xfmDigitalAirStrike
where extract( year from surveysentts::date) = 2014
  and extract(month from surveysentts::date)= 6

 -- 2. delete those rows from xfmDigitalAirStrike
delete from np.xfmDigitalAirStrike
where extract( year from surveysentts::date) = 2014
  and extract(month from surveysentts::date)= 6

-- 3. when i do it again, have some overlap so i can see if update works  
--      also, rig up some anomalies      
*/
 -- now, sp processNewSurveyData should work

-- surveys IN stgDigitalAirStrike that DO NOT exist IN xfmDigitalAirStrike 
DELETE FROM np.xfmNewSurveys;     
INSERT INTO np.xfmNewSurveys (
  DealerName,VIN,RONumber,SurveySentTS,TransactionType,CustomerName,
  CustomerEmail,UnsubscribeTS,SurveyResponseTS,ConsentToPublish,
  StarRating,ReferralLikelihood,CustomerExperience,PassProfanity,
  DealerResponseTS,DealerResponse,SalesPersonName,PublicationDate,
  PublicationStatus, CustomerSuggestion) 
SELECT a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,a.CustomerExperience,a.PassProfanity,
  a.DealerResponseTS,a.DealerResponse,a.SalesPersonName,a.PublicationDate,
  a.PublicationStatus, a.CustomerSuggestion 
FROM np.stgDigitalAirStrike a
WHERE NOT EXISTS (
  SELECT 1
  FROM np.xfmDigitalAirStrike
  WHERE vin = a.vin
    AND transactionType = a.TransactionType
    AND SurveySentTS = a.SurveySentTS); 

 -- INSERT INTO xfmAnomalies FROM xfmNewSurveys WHERE there IS more than one row per VIN/TranType/SentTS
INSERT INTO np.xfmAnomalies  
SELECT current_date, 'New',a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,a.CustomerExperience,a.PassProfanity,
  a.DealerResponseTS,a.DealerResponse,a.SalesPersonName,a.PublicationDate,
  a.PublicationStatus, a.CustomerSuggestion
FROM np.xfmNewSurveys a 
WHERE EXISTS (
  SELECT 1
  FROM (   
    SELECT vin, TransactionType, SurveySentTS
    FROM np.xfmNewSurveys
    GROUP BY vin, TransactionType, SurveySentTS
    HAVING COUNT(*) > 1) b 
  WHERE b.vin = a.vin
    AND b.TransactionType = a.TransactionType
    AND b.SurveySentTS = a.SurveySentTS);      

-- existing surveys
DELETE FROM np.xfmSurveysToUpdate;     
INSERT INTO np.xfmSurveysToUpdate (
  DealerName,VIN,RONumber,SurveySentTS,TransactionType,CustomerName,
  CustomerEmail,UnsubscribeTS,SurveyResponseTS,ConsentToPublish,
  StarRating,ReferralLikelihood,CustomerExperience,PassProfanity,
  DealerResponseTS,DealerResponse,SalesPersonName,PublicationDate,
  PublicationStatus, CustomerSuggestion)
SELECT a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,a.CustomerExperience,a.PassProfanity,
  a.DealerResponseTS,a.DealerResponse,a.SalesPersonName,a.PublicationDate,
  a.PublicationStatus, a.CustomerSuggestion 
FROM np.stgDigitalAirStrike a
WHERE EXISTS (
  SELECT 1
  FROM np.xfmDigitalAirStrike
  WHERE vin = a.vin
    AND transactionType = a.TransactionType
    AND SurveySentTS = a.SurveySentTS);        

-- INSERT INTO xfmAnomalies FROM xfmSurveysToUpdate WHERE there IS more than one row per VIN/TranType/SentTS 
INSERT INTO np.xfmAnomalies  
SELECT current_date, 'New',a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,a.CustomerExperience,a.PassProfanity,
  a.DealerResponseTS,a.DealerResponse,a.SalesPersonName,a.PublicationDate,
  a.PublicationStatus, a.CustomerSuggestion
FROM np.xfmSurveysToUpdate a 
WHERE EXISTS (
  SELECT 1
  FROM (   
    SELECT vin, TransactionType, SurveySentTS
    FROM np.xfmSurveysToUpdate
    GROUP BY vin, TransactionType, SurveySentTS
    HAVING COUNT(*) > 1) b 
  WHERE b.vin = a.vin
    AND b.TransactionType = a.TransactionType
    AND b.SurveySentTS = a.SurveySentTS);      

-- INSERT new rows
INSERT INTO np.xfmDigitalAirStrike (
  DealerName,VIN,RONumber,SurveySentTS,TransactionType,CustomerName,
  CustomerEmail,UnsubscribeTS,SurveyResponseTS,ConsentToPublish,
  StarRating,ReferralLikelihood,CustomerExperience,PassProfanity,
  DealerResponseTS,DealerResponse,SalesPersonName,PublicationDate,
  PublicationStatus, CustomerSuggestion)
SELECT a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,a.CustomerExperience,a.PassProfanity,
  a.DealerResponseTS,a.DealerResponse,a.SalesPersonName,a.PublicationDate,
  a.PublicationStatus, a.CustomerSuggestion
FROM np.xfmNewSurveys a 
WHERE NOT EXISTS ( -- exclude WHERE there IS more than one row per VIN/TranType/SentTS 
  SELECT 1
  FROM (   
    SELECT vin, TransactionType, SurveySentTS
    FROM np.xfmNewSurveys
    GROUP BY vin, TransactionType, SurveySentTS
    HAVING COUNT(*) > 1) b 
  WHERE b.vin = a.vin
    AND b.TransactionType = a.TransactionType
    AND b.SurveySentTS = a.SurveySentTS);      







/*
-- modify some xfmSurveysToUpdate data
select * 
-- select count(*)
from np.xfmSurveysToUpdate
where customersuggestion is null 
  and extract(year from surveyresponsets::date) <> 1969

update np.xfmSurveysToUpdate
  set customersuggestion = 'four score and seven years ago'
where customersuggestion is null 
  and extract(year from surveyresponsets::date) <> 1969;
*/

-- UPDATE existing rows with the new values   
UPDATE np.xfmDigitalAirStrike
  SET UnsubscribeTS = c.UnsubscribeTS,
      SurveyResponseTS = c.SurveyResponseTS,
      ConsentToPublish = c.ConsentToPublish,
      StarRating = c.StarRating,
      ReferralLikelihood = c.ReferralLikelihood,
      CustomerExperience = c.CustomerExperience,
      PassProfanity = c.PassProfanity,
      DealerResponseTS = c.DealerResponseTS,
      DealerResponse = c.DealerResponse,
      SalesPersonName = c.SalesPersonName,
      PublicationDate = c.PublicationDate,
      PublicationStatus = c.PublicationStatus,
      CustomerSuggestion = c.CustomerSuggestion  
FROM (      
  SELECT a.VIN, a.SurveySentTS,a.TransactionType,
    b.UnsubscribeTS,b.SurveyResponseTS,b.ConsentToPublish,
    b.StarRating,b.ReferralLikelihood,b.CustomerExperience,b.PassProfanity,
    b.DealerResponseTS,b.DealerResponse,b.SalesPersonName,b.PublicationDate,
    b.PublicationStatus, b.CustomerSuggestion 
  FROM np.xfmDigitalAirStrike a
  INNER JOIN np.xfmSurveysToUpdate b on a.vin = b.vin
    AND a.TransactionType = b.TransactionType
    AND a.SurveySentTS = b.SurveySentTS 
  WHERE NOT EXISTS ( -- exclude WHERE there IS more than one row per VIN/TranType/SentTS 
    SELECT 1
    FROM (   
      SELECT vin, TransactionType, SurveySentTS
      FROM np.xfmSurveysToUpdate
      GROUP BY vin, TransactionType, SurveySentTS
      HAVING COUNT(*) > 1) b 
    WHERE b.vin = a.vin
      AND b.TransactionType = a.TransactionType
      AND b.SurveySentTS = a.SurveySentTS)) c
WHERE np.xfmDigitalAirStrike.vin = c.vin
  AND np.xfmDigitalAirStrike.TransactionType = c.TransactionType
  AND np.xfmDigitalAirStrike.SurveySentTS = c.SurveySentTS; 

INSERT INTO np.dashistory
SELECT *
FROM np.stgDigitalAirStrike a
WHERE NOT EXISTS (
  SELECT 1
  FROM np.dashistory 
  WHERE SurveyID = a.SurveyID);    

select count(*)
from np.xfmAnomalies
where theDate = current_date;    

$$
language sql;  


select np.processNewSurveyData();
