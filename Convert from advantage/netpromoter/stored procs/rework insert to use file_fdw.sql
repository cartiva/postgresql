﻿/*
-- replace all this with

CREATE OR REPLACE FUNCTION netpromoter.insertdasrawhondaline(dealername text, vin text, ronumber text, surveysentts timestamp without time zone, transactiontype text, customername text, customeremail text, unsubscribets timestamp without time zone, surveyresponsets timestamp without time zone, consenttopublish text, starrating integer, referrallikelihood integer, customerexperience text, customersuggestion text, salespersonname text)
  RETURNS void AS
$BODY$
  
insert into netpromoter.stgDigitalAirStrike (SurveyID, DealerName, VIN, RONumber, SurveySentTS,
	  TransactionType, CustomerName, CustomerEmail, UnsubscribeTS, SurveyResponseTS, ConsentToPublish, StarRating,
	  ReferralLikelihood, CustomerExperience, CustomerSuggestion, SalesPersonName)
values(netpromoter.uuid_generate_v4(),$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)
$BODY$
  LANGUAGE sql VOLATILE;

CREATE OR REPLACE FUNCTION netpromoter.insertdasrawline(dealername text, vin text, ronumber text, surveysentts timestamp without time zone, transactiontype text, customername text, customeremail text, unsubscribets timestamp without time zone, surveyresponsets timestamp without time zone, consenttopublish text, starrating integer, referrallikelihood integer, customerexperience text, passprofanity text, dealerresponsets timestamp without time zone, dealerresponse text, salespersonname text, publicationdate date, publicationstatus text)
  RETURNS void AS
$BODY$

insert into netpromoter.stgDigitalAirStrike (SurveyID, DealerName, VIN, 
    RONumber, SurveySentTS,TransactionType, CustomerName, CustomerEmail, 
    UnsubscribeTS, SurveyResponseTS, ConsentToPublish, StarRating,
	  ReferralLikelihood, CustomerExperience, PassProfanity, 
	  DealerResponseTS, DealerResponse, SalesPersonName, PublicationDate, 
	  PublicationStatus)
values(netpromoter.uuid_generate_v4(),$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,
  $12,$13,$14,$15,$16,$17,$18,$19)
$BODY$
  LANGUAGE sql VOLATILE;

-- with this
*/  


--delete from np.stgDigitalAirStrike;
--insert into np.stgDigitalAirStrike
select uuid_generate_v4(), dealerName, vin, ronumber, 
  case when surveyResponseTS = '' then null else surveyResponseTS::timestamp end,
  transactionType, customerName, customerEmail, 
  case when unsubscribeTS = '' then null else unsubscribeTS::timestamp end,
  case when surveyResponseTS = '' then null else surveyResponseTS::timestamp end,
  consentToPublish, 
  case when starrating = '' then null else starrating::int end,
  case when referralLikelihood = '' then null else referralLikelihood::int end,
  customerExperience, passProfanity, 
  case when dealerResponseTS = '' then null else dealerResponseTS::timestamp end,
  dealerResponse, salesPersonName, 
  case when publicationDate = '' then null else publicationDate::date end,
  publicationStatus, 
  null as customerSuggestion
from np.extDigitalAirStrikeRY1
union
select uuid_generate_v4(), dealerName, vin, ronumber, 
  case when surveyResponseTS = '' then null else surveyResponseTS::timestamp end,
  transactionType, customerName, customerEmail, 
  case when unsubscribeTS = '' then null else unsubscribeTS::timestamp end,
  case when surveyResponseTS = '' then null else surveyResponseTS::timestamp end,
  consentToPublish, 
  case when starrating = '' then null else starrating::int end,
  case when referralLikelihood = '' then null else referralLikelihood::int end,
  customerExperience, 
  null as passProfanity, 
  null as dealerResponseTS,
  null as dealerResponse , salesPersonName, 
  null as publicationDate,
  null as publicationStatus, 
  customerSuggestion
from np.extDigitalAirStrikeRY2;


