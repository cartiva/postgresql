/*
CREATE OR REPLACE FUNCTION
RETURNS
AS $$
DECLARE
BEGIN RETURN
END;
$$
LANGUAGE PLPGSQL;
*/

drop FUNCTION np.insertDasRawHondaLine (
      dealername text,
      VIN text,
      RONumber text,
      SurveySentTS timestamp,
      TransactionType text,
      CustomerName text,
      CustomerEmail text,
      UnsubscribeTS timestamp,
      SurveyResponseTS timestamp,
      ConsentToPublish text,
      StarRating Integer,
      ReferralLikelihood Integer,
      CustomerExperience text,
      CustomerSuggestion text,
      SalesPersonName text);
           
CREATE OR REPLACE FUNCTION np.insertDasRawHondaLine (
      dealername text,
      VIN text,
      RONumber text,
      SurveySentTS timestamp,
      TransactionType text,
      CustomerName text,
      CustomerEmail text,
      UnsubscribeTS timestamp,
      SurveyResponseTS timestamp,
      ConsentToPublish text,
      StarRating Integer,
      ReferralLikelihood Integer,
      CustomerExperience text,
      CustomerSuggestion text,
      SalesPersonName text)      
RETURNS VOID
AS $$
/*
in the orig ads version, returns a logical (inserted) which is simply:
  Insert into __Output values(True);
  this seems superfluous to me, that value is not used or checked 
  anywhere that i can find in the code
*/  
insert into np.stgDigitalAirStrike (SurveyID, DealerName, VIN, 
          RONumber, SurveySentTS, TransactionType, CustomerName, 
          CustomerEmail, UnsubscribeTS, SurveyResponseTS, 
          ConsentToPublish, StarRating, ReferralLikelihood, 
          CustomerExperience, CustomerSuggestion, SalesPersonName)
values(uuid_generate_v4(),$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)
$$
LANGUAGE SQL;     

select np.insertDasRawHondaLine('Rydell', '1GCE', 
  '123456', '05/01/2014 08:30:00', 'Sales','Jeff Olson', 
  'jolson@customeremail.com', null, '05/30/2014 13:10:10',
  'Yes', 5, 10, 
  'It sucked', 'Fuckin awesome', 'Larry Humble');

/* *************************************************************************************************************************** */

drop FUNCTION np.insertDasRawLine (
      DealerName text,
      VIN text,
      RONumber text,
      SurveySentTS TIMESTAMP,
      TransactionTypetext,
      CustomerName text,
      CustomerEmail text,
      UnsubscribeTS TIMESTAMP,
      SurveyResponseTS TIMESTAMP,
      ConsentToPublish text,
      StarRating Integer,
      ReferralLikelihood Integer,
      CustomerExperience text,
      PassProfanity text,
      DealerResponseTS TIMESTAMP,
      DealerResponse text,
      SalesPersonName text,
      PublicationDate DATE,
      PublicationStatustext);
           
CREATE OR REPLACE FUNCTION np.insertDasRawLine (
      DealerName text,
      VIN text,
      RONumber text,
      SurveySentTS TIMESTAMP,
      TransactionType text,
      CustomerName text,
      CustomerEmail text,
      UnsubscribeTS TIMESTAMP,
      SurveyResponseTS TIMESTAMP,
      ConsentToPublish text,
      StarRating Integer,
      ReferralLikelihood Integer,
      CustomerExperience text,
      PassProfanity text,
      DealerResponseTS TIMESTAMP,
      DealerResponse text,
      SalesPersonName text,
      PublicationDate DATE,
      PublicationStatus text) 
RETURNS VOID 
AS $$
/*
in the orig ads version, returns a logical (inserted) which is simply:
  Insert into __Output values(True);
  this seems superfluous to me, that value is not used or checked 
  anywhere that i can find in the code
*/  
insert into np.stgDigitalAirStrike (SurveyID, DealerName, VIN, 
          RONumber, SurveySentTS,TransactionType, CustomerName, CustomerEmail, 
          UnsubscribeTS, SurveyResponseTS, ConsentToPublish, StarRating,
	  ReferralLikelihood, CustomerExperience, PassProfanity, 
	  DealerResponseTS, DealerResponse, SalesPersonName, PublicationDate, 
	  PublicationStatus)
values(uuid_generate_v4(),$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,
  $12,$13,$14,$15,$16,$17,$18,$19)
$$
LANGUAGE SQL;     

select np.insertDasRawLine('Rydell', '1GCE', 
  '123456', '05/01/2014 08:30:00', 'Sales', 'Jeff Olson', 'jolson@customeremail.com', 
  null, '05/30/2014 13:10:10', 'Yes', 5, 
  10, 'It sucked', 'pass profanity',
  '06/02/2014 15:10:10','Dealer Response', 'Larry Humble', '06/01/2014',
  'publication status');
  
/* *************************************************************************************************************************** */

--appears that sp InsertDASSurvey is not used

/* *************************************************************************************************************************** */

drop function np.npFromThruDates (out fromDate date, out thruDate date);

create or replace function np.npFromThruDates (out fromDate date, out thruDate date)
returns setof record
as $$
select b.theDate as fromDate, c.theDate as thruDate
from (
  select max(theDate) - 90 as fromDate, max(thedate) as thruDate
  from dds.day
  where dayOfWeek = 1
    and theDate <= current_date) a
left join dds.day b on a.fromDate = b.thedate
left join dds.day c on a.thruDate = c.thedate;    
$$
language sql;

-- call it this way, returns 2 columns
select * from np.npFromThruDates();

-- if called this way
select np.npFromThruDates();
--it returns the 2 date values in an array


