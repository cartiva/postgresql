CREATE OR REPLACE FUNCTION
RETURNS
AS $$
DECLARE
BEGIN RETURN
END;
$$
LANGUAGE PLPGSQL;

--had to change sent, returned from int to bigint
-- language to sql from plsql
-- but what it returns is (3959,568,0.14,0.65), instead of individual columns
-- change the output type to TABLE
-- 6/1 where i left off, not getting a table result but an array, wtf
-- holy fucking shit, it is in the call, needs to be select *, i was doing, select np.getStoreScore('RY1');

-- getDepartmentPeopleScores
-- DROP FUNCTION np.getDepartmentPeopleScores (_store text, _department text);
CREATE OR REPLACE FUNCTION np.getDepartmentPeopleScores (_store text, _department text)
RETURNS table (
  name text,
  sent bigint,
  returned bigint,
  responseRate numeric,
  score numeric)
AS $$
  declare _fromDate date := (
    select max(x.theDate) - 90 
    from dds.day x
    where dayofweek = 1
    and x.thedate <= current_date);
  declare _thruDate date := (
    select max(y.theDate) 
    from dds.day y
    where dayofweek = 1
    and y.thedate <= current_date); 
BEGIN RETURN QUERY
	SELECT a.employeeName, COUNT(*) AS sent,
	  SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) AS returned,    
	  CASE COUNT(*)
	    WHEN 0 THEN 0
	    ELSE 
	      ROUND(100 * (SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) * .01/COUNT(*)), 2)
	  END AS responseRate,
	  ROUND(CASE SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) 
	    WHEN 0 THEN 0
	    ELSE 
	      (SUM(CASE WHEN a.referralLikelihood > 8 THEN 1 ELSE 0 END) * 
		100/SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) 
	      -
	      SUM(CASE WHEN a.referralLikelihood < 7 AND a.customerResponse = true THEN 1 ELSE 0 END) * 100
		/SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END))/100.0
	  END, 2)  AS Score      
	FROM np.cstfbNpData a
	WHERE a.transactionDate BETWEEN _fromDate AND _thruDate
	  AND lower(a.storeCode) = lower(_store)
	  AND 
	    CASE upper(_department)
	      WHEN 'SALES' THEN a.transactionType = 'Sales'
	      WHEN 'SERVICE' THEN a.transactionType = 'Service'
	      WHEN 'MAINSHOP' THEN a.subDepartment = 'MR'
	      WHEN 'PDQ' THEN a.subDepartment = 'QL'
	      WHEN 'BODYSHOP' THEN a.subDepartment = 'BS'
	    END
	GROUP BY a.employeeName; 
END;        
$$
language plpgsql;  

select * from np.getDepartmentPeopleScores ('RY1', 'sales');

-- getDepartmentScores
-- drop function np.getDepartmentScores (_store text, _department text)
CREATE OR REPLACE FUNCTION np.getDepartmentScores (_store text, _department text)
RETURNS table (
  sent bigint,
  returned bigint,
  responseRate numeric,
  score numeric)
AS $$
  declare _fromDate date := (
    select max(x.theDate) - 90 
    from dds.day x
    where dayofweek = 1
    and x.thedate <= current_date);
  declare _thruDate date := (
    select max(y.theDate) 
    from dds.day y
    where dayofweek = 1
    and y.thedate <= current_date); 
BEGIN RETURN QUERY
	SELECT COUNT(*) AS sent,
	  SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) AS returned,    
	  CASE COUNT(*)
	    WHEN 0 THEN 0
	    ELSE 
	      ROUND(100 * (SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) * .01/COUNT(*)), 2)
	  END AS responseRate,
	  ROUND(CASE SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) 
	    WHEN 0 THEN 0
	    ELSE 
	      (SUM(CASE WHEN a.referralLikelihood > 8 THEN 1 ELSE 0 END) * 
		100/SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) 
	      -
	      SUM(CASE WHEN a.referralLikelihood < 7 AND a.customerResponse = true THEN 1 ELSE 0 END) * 100
		/SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END))/100.0
	  END, 2)  AS Score      
	FROM np.cstfbNpData a
	WHERE a.transactionDate BETWEEN _fromDate AND _thruDate
	  AND lower(a.storeCode) = lower(_store)
	  AND 
	    CASE upper(_department)
	      WHEN 'SALES' THEN a.transactionType = 'Sales'
	      WHEN 'SERVICE' THEN a.transactionType = 'Service'
	      WHEN 'MAINSHOP' THEN a.subDepartment = 'MR'
	      WHEN 'PDQ' THEN a.subDepartment = 'QL'
	      WHEN 'BODYSHOP' THEN a.subDepartment = 'BS'
	    END; 
END;        
$$
language plpgsql;  

select * from np.getDepartmentScores ('RY2', 'mainshop');

-- getDepartmentScoresRolling
-- drop FUNCTION np.getDepartmentScoresRolling (_store text, _department text);
CREATE OR REPLACE FUNCTION np.getDepartmentScoresRolling (_store text, _department text)
RETURNS table (
  fromDate date,
  thruDate date,
  responseRate numeric,
  score numeric) AS $$
BEGIN RETURN QUERY
	SELECT a.fromDate, a.thruDate, a.responseRate, a.score
	FROM np.cstfbNpDataRolling a
	WHERE lower(a.storeCode) = 'ry1' -- lower(_store)
	  AND a.thruDate IN (
	    SELECT x.theDate as mondays
	    FROM dds.day x
	    WHERE x.dayofWeek = 2
	      AND x.theDate <= current_date
	    ORDER BY x.theDate DESC
	    LIMIT 12)
	  AND 
	    CASE upper(_department)
	      WHEN 'SALES' THEN department = 'Sales' AND subDepartment = 'All'
	      WHEN 'SERVICE' THEN department = 'Service' AND subDepartment = 'All'
	      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
	      WHEN 'PDQ' THEN subDepartment = 'QL'
	      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
	    END
	ORDER BY a.thruDate
	LIMIT 100;	
END;
$$
language plpgsql;

select * from np.getDepartmentScoresRolling('RY2', 'MainShop');

-- getDepartmentSurveys
-- drop FUNCTION np.getDepartmentSurveys (_store text, _department text);
CREATE OR REPLACE FUNCTION np.getDepartmentSurveys (_store text, _department text)
RETURNS table (
  employee text,
  theDate date,
  ro_Stock text,
  customerName text,
  referralLikelihood integer,
  customerExperience text)
AS $$
DECLARE _fromDate date := (
  select max(x.theDate) - 90 
  from dds.day x
  where dayofweek = 1
  and x.thedate <= current_date);
DECLARE _thruDate date := (
  select max(y.theDate) 
  from dds.day y
  where dayofweek = 1
  and y.thedate <= current_date); 
BEGIN RETURN QUERY
	SELECT a.employeeName, a.transactionDate, a.transactionNumber, a.customerName, 
	  a.referralLikelihood, a.customerExperience  
	FROM np.cstfbNpData a
	WHERE a.transactionDate BETWEEN _fromDate AND _thruDate
	  AND a.customerResponse = true
	  AND lower(a.storeCode) = lower(_store)
	  AND 
	    CASE upper(_department)
	      WHEN 'SALES' THEN transactionType = 'Sales'
	      WHEN 'SERVICE' THEN transactionType = 'Service'
	      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
	      WHEN 'PDQ' THEN subDepartment = 'QL'
	      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
	    END;
END;        
$$
language plpgsql;  

select * from np.getDepartmentSurveys('RY1', 'BodYShop');

-- getEmployeeSurveys
-- works great
-- MMMUUUSSSTTTT qualify everything to disambiguate parameters/variables/column names
-- did a lower() on where clause, otherwise case sensitive
/*
variables
  go before BEGIN
  can not be preceded with @

*/ 
-- drop FUNCTION np.getEmployeeSurveys (_employee text)
CREATE OR REPLACE FUNCTION np.getEmployeeSurveys (_employee text)
RETURNS table (
  employee text,
  theDate date,
  ro_Stock text,
  customerName text,
  referralLikelihood integer,
  customerExperience text)
AS $$
  declare _fromDate date := (
    select max(x.theDate) - 90 
    from dds.day x
    where dayofweek = 1
    and x.thedate <= current_date);
  declare _thruDate date := (
    select max(y.theDate) 
    from dds.day y
    where dayofweek = 1
    and y.thedate <= current_date); 
BEGIN RETURN QUERY
  select a.employeename, a.transactiondate, a.transactionNumber, a.customerName,
    a.referralLikelihood, a.customerExperience
  from np.cstfbNpData a
  where a.transactionDate between _fromDate and _thruDate
    and lower(a.employeeName) = lower(_employee);
END;        
$$
language plpgsql;  

select * from np.getEmployeeSurveys ('nicholas Maro');


-- getMarketScores

CREATE OR REPLACE FUNCTION np.getMarketScores()
RETURNS TABLE (
  sent bigint,
  returned bigint,
  responseRate numeric,
  score numeric)
AS $$
DECLARE _fromDate date := (
  select max(theDate) - 90 
  from dds.day
  where dayofweek = 1
  and thedate <= current_date);
DECLARE _thruDate date := (
  select max(theDate) 
  from dds.day
  where dayofweek = 1
  and thedate <= current_date);   
BEGIN RETURN QUERY
	SELECT COUNT(*) AS sent,
	  SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) AS returned,    
	  CASE COUNT(*)
	    WHEN 0 THEN 0
	    ELSE 
	      ROUND(100 * (SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) * .01/COUNT(*)), 2)
	  END AS responseRate,
	  ROUND(CASE SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) 
	    WHEN 0 THEN 0
	    ELSE 
	      (SUM(CASE WHEN a.referralLikelihood > 8 THEN 1 ELSE 0 END) * 
		100/SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END) 
	      -
	      SUM(CASE WHEN a.referralLikelihood < 7 AND a.customerResponse = true THEN 1 ELSE 0 END) * 100
		/SUM(CASE WHEN a.customerResponse = true THEN 1 ELSE 0 END))/100.0
	  END, 2)  AS Score      
	FROM np.cstfbNpData a
	WHERE a.transactionDate BETWEEN _fromDate AND _thruDate;
END;
$$
LANGUAGE PLPGSQL;

select * from np.getMarketScores();


-- getMarketScoresRolling
-- drop FUNCTION np.getMarketScoresRolling();
CREATE OR REPLACE FUNCTION np.getMarketScoresRolling()
RETURNS table (
  fromDate date,
  thruDate date,
  responseRate numeric,
  score numeric) AS $$
BEGIN RETURN QUERY
	SELECT a.fromDate, a.thruDate, a.responseRate, a.score
	FROM np.cstfbNpDataRolling a
	WHERE lower(a.storeCode) = 'all'
	  AND a.thruDate IN (
	    SELECT x.theDate as mondays
	    FROM dds.day x
	    WHERE x.dayofWeek = 2
	      AND x.theDate <= current_date
	    ORDER BY x.theDate DESC
	    LIMIT 12)
	ORDER BY a.thruDate
	LIMIT 100;	
END;
$$
language plpgsql;

select * from np.getMarketScoresRolling();

-- getMarketSurveys
-- drop FUNCTION np.getMarketSurveys();
CREATE OR REPLACE FUNCTION np.getMarketSurveys()
RETURNS TABLE (
  employee text,
  theDate date,
  ro_Stock text,
  customerName text,
  referralLikelihood integer,
  customerExperience text)
AS $$
DECLARE _fromDate date := (
  select max(x.theDate) - 90 
  from dds.day x
  where x.dayofweek = 1
  and x.thedate <= current_date);
DECLARE _thruDate date := (
  select max(y.theDate) 
  from dds.day y
  where y.dayofweek = 1
  and y.thedate <= current_date);   
BEGIN RETURN QUERY
  select a.employeename, a.transactiondate, a.transactionNumber, a.customerName,
    a.referralLikelihood, a.customerExperience
  from np.cstfbNpData a
  where a.transactionDate between _fromDate and _thruDate
    and a.customerResponse = true;
END;
$$
LANGUAGE PLPGSQL;

select * from np.getMarketSurveys();

-- getStoreScore
-- drop function np.getStoreScore (IN sc text);
create or replace function np.getStoreScore (
  IN sc text)
  RETURNS TABLE (
    sent bigint,
    returned bigint,
    responseRate numeric,
    core numeric)
  AS $$
    BEGIN RETURN QUERY
	SELECT COUNT(*) AS sent,
	  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
	  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * .01/COUNT(*)), 2)  AS responseRate,
	  round((SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
	  -
	  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100 /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0, 2) as score
	FROM np.cstfbNpData
	WHERE transactionDate BETWEEN (
	  SELECT MAX(thedate) - 90 AS fromDate
	  FROM dds.day
	  WHERE dayofweek = 1
	    AND thedate <= current_date) 
	      and (
	  SELECT MAX(thedate)AS thruDate
	  FROM dds.day
	  WHERE dayofweek = 1
	    AND thedate <= current_date)       
	AND storeCode = $1;
    END;
$$
language plpgsql;

select * from np.getStoreScore('RY1');

-- getStoreScoresRolling
CREATE OR REPLACE FUNCTION np.getStoreScoresRolling(_store text)
RETURNS TABLE (
  fromDate date,
  thruDate date,
  responseRate numeric,
  score numeric)
AS $$
BEGIN RETURN QUERY
	SELECT a.fromDate, a.thruDate, a.responseRate, a.score
	FROM np.cstfbNpDataRolling a
	WHERE lower(a.storeCode) = lower(_store)
	  AND lower(a.department) = 'all'
	  AND a.thruDate IN (
	    SELECT x.theDate as mondays
	    FROM dds.day x
	    WHERE x.dayofWeek = 2
	      AND x.theDate <= current_date
	    ORDER BY x.theDate DESC
	    LIMIT 12)
	ORDER BY a.thruDate
	LIMIT 100;	
END;
$$
LANGUAGE PLPGSQL;

select * from np.getStoreScoresRolling('RY2');

-- getStoreSurveys
-- drop FUNCTION np.getStoreSurveys(_store text);
CREATE OR REPLACE FUNCTION np.getStoreSurveys(_store text)
RETURNS TABLE (
  employee text,
  theDate date,
  ro_Stock text,
  customerName text,
  reverralLikelihood integer,
  customerExperience text)
AS $$
/*
will this retain comments in the definition sql
yep, it does, if the comments are within the body
*/
DECLARE _fromDate date := (
  select max(x.theDate) - 90 
  from dds.day x
  where x.dayofweek = 1
  and x.thedate <= current_date);
DECLARE _thruDate date := (
  select max(y.theDate) 
  from dds.day y
  where y.dayofweek = 1
  and y.thedate <= current_date);  
BEGIN RETURN QUERY 
	select a.employeename, a.transactiondate, a.transactionNumber, a.customerName,
	  a.referralLikelihood, a.customerExperience
	from np.cstfbNpData a
	where a.transactionDate between _fromDate and _thruDate
	  and a.customerResponse = true
	  AND lower(storeCode) = lower(_store);
END;
$$
LANGUAGE PLPGSQL;

select * from np.getStoreSurveys('Ry1');

