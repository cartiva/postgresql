﻿create or replace function np.populateCstfbNpData()
returns void
as $$

/*
postgres diff from ads
equality:
  lower(a.transactiontype) = 'service'
  ) n on trim(m.transactionNumber) = trim(n.ro);
timestamp to date:  
  SELECT surveySentTs::date AS transactionDate
year from trimestamp:
  WHEN extract(year from surveyresponsets)  <> 1969  
*/
  DELETE FROM np.cstfbNpData;
  -- sales
  INSERT INTO np.cstfbNpData
 SELECT m.transactionDate, m.customerResponse, m.storeCode, 'None', n.fullname,
    n.stocknumber, 'Sales', m.customerName, m.referralLikelihood,
    m.customerExperience
  FROM (    
    SELECT surveySentTs::date AS transactionDate,
      CASE WHEN extract(year from surveyresponsets)  <> 1969 THEN true 
        ELSE false END AS customerResponse,
      CASE dealername
        WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
        ELSE 'RY2'
      END AS storeCode,
      roNumber AS transactionNumber, a.vin, a.transactionType, a.customerName,
      referralLikelihood, customerExperience
    FROM np.xfmDigitalAirStrike a
    INNER JOIN ( -- remove duplicate surveys
      SELECT max(surveySentTS::date) AS theDate, 
        customerName, vin, transactionType
      FROM np.xfmDigitalAirStrike
       WHERE lower(transactionType) = 'sales'
      GROUP BY customerName, vin, transactionType) b 
        on a.surveySentTS::date = b.theDate
          AND a.customerName = b.customerName
          AND a.transactionType = b.transactionType
          AND a.vin = b.vin
    WHERE a.transactionType = 'Sales') m  
  LEFT JOIN (
    SELECT b.stocknumber, c.vin, d.fullname, dd.thedate AS capDate, 
      ddd.thedate AS appDate, dddd.thedate AS origDate
    FROM dds.factVehicleSale b
    INNER JOIN dds.dimVehicle c on b.vehicleKey = c.vehicleKey
    INNER JOIN dds.dimSalesPerson d on b.consultantKey = d.salesPersonKey
    INNER JOIN dds.day dd on b.cappedDateKey = dd.datekey
    INNER JOIN dds.day ddd on b.approveddatekey = ddd.datekey
    INNER JOIN dds.day dddd on b.originationdatekey = dddd.datekey) n 
      on m.vin = n.vin
        AND (ABS(m.transactionDate - n.capdate) < 14  
          OR ABS(m.transactiondate - n.appdate) < 14 
          OR ABS(m.transactiondate - n.origdate) < 14
          OR m.transactionDate = '02/08/2014')
  WHERE n.fullname IS NOT NULL;    
  
  -- service  
  INSERT INTO np.cstfbNpData    
  SELECT m.transactionDate, m.customerResponse, m.storeCode, 
    coalesce(n.censusDept, 'XX'), 
    coalesce(n.fullname, 'Other'), m.transactionNumber, 'Service', 
    m.customerName, m.referralLikelihood, m.customerExperience
  FROM (   
    SELECT surveySentTs::date AS transactionDate,  
      CASE WHEN extract(year from surveyResponseTS) <> 1969 THEN true 
        ELSE false END AS customerResponse,
      CASE dealername
        WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
        ELSE 'RY2'
      END AS storeCode,
      'subDepartment', 'employeename',
      left(roNumber, 9) AS transactionNumber, a.vin, a.transactionType, a.customerName,
      referralLikelihood, customerExperience
    FROM np.xfmDigitalAirStrike a
    WHERE lower(a.transactiontype) = 'service'
      AND roNumber NOT IN ( -- eliminate dup ro
        SELECT roNumber
        FROM np.xfmdigitalairstrike
        GROUP BY roNumber
        HAVING COUNT(*) > 1)) m
  LEFT JOIN (
    SELECT distinct b.ro, c.censusDept, TRIM(d.firstname) || ' ' || TRIM(d.lastname) AS fullName
    FROM dds.factRepairOrder b
    INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.serviceWriterKey
    LEFT JOIN dds.edwEmployeeDim d 
  --    on c.storecode = d.storecode -- can't use store, writers may NOT be emp of the store
      ON c.employeenumber = d.employeenumber 
      AND d.currentrow = true 
    WHERE b.ro IN (
      SELECT ronumber
      FROM np.xfmdigitalairstrike )) n on trim(m.transactionNumber) = trim(n.ro);  

  DELETE FROM np.cstfbNpDataRolling;		
  INSERT INTO np.cstfbNpDataRolling  
  -- market
  SELECT 'GF' AS market, 'All' as store, 'All' as department, 'All' as subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
    round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
      .01/COUNT(*)), 2)  AS responseRate,
      
    round((SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
    -
    SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
      /SUM(CASE WHEN customerResponse THEN 1 ELSE 0 END))/100.0, 2) AS score
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= current_date
      ORDER BY theDate DESC limit 20) a) b
  LEFT JOIN np.cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  GROUP BY fromDate, thruDate order by fromdate;     

  INSERT INTO np.cstfbNpDataRolling  
  -- store
  SELECT 'GF' as market, storecode AS store, 'All' as department, 'All' as subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
    round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
      .01/COUNT(*)), 2)  AS responseRate,
    (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
    -
    SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
      /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= current_date
      ORDER BY theDate DESC limit 20) a) b
  LEFT JOIN np.cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  GROUP BY storecode, fromDate, thruDate;

  INSERT INTO np.cstfbNpDataRolling  
  -- department
  SELECT 'GF' as market, storecode AS store, left(transactionType, 12) as department, 'All' as subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
    round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
      .01/COUNT(*)), 2)  AS responseRate,
    (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
    -
    SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
      /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= current_date
      ORDER BY theDate DESC limit 20) a) b
  LEFT JOIN np.cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  GROUP BY storecode, transactionType, fromDate, thruDate;  

  INSERT INTO np.cstfbNpDataRolling  
  -- subDepartment
  SELECT 'GF' as market, storecode AS store, left(transactionType, 12) as department, 
    subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
    round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
        .01/COUNT(*)), 2) AS responseRate,
    (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
          100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
        -
        SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
          /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
  -- SELECT *    
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= current_date
      ORDER BY theDate DESC limit 20) a) b
  LEFT JOIN np.cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  WHERE c.subDepartment IN ('BS','MR','QL')
  GROUP BY storecode, transactionType, subDepartment, fromDate, thruDate;
$$
language sql; 


/*
select np.populateCstfbNpData();

select * from np.getDepartmentScoresRolling('ry1','bodyshop');

select * from np.getMarketScoresRolling()

select * from np.getStoreScoresRolling('ry1')

*/






    