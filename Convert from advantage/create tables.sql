/*
-- test select 123
SELECT LEFT(a.name, 50) AS tableName, LEFT(table_primary_key, 50) AS primaryKey,
  left(b.name, 50) AS columnName, b.field_num, b.field_type, b.field_Length, 
  field_decimal, field_can_be_null,
  field_default_value
FROM system.tables a
LEFT JOIN system.columns b ON a.name = b.parent
ORDER BY tableName, b.field_num

SELECT DISTINCT field_type FROM system.columns;

SELECT LEFT(a.name, 50) AS tableName, LEFT(table_primary_key, 50) AS primaryKey,
  left(b.name, 50) AS columnName, b.field_num, b.field_type, b.field_Length, 
  field_decimal, field_can_be_null,
  field_default_value
FROM system.tables a
LEFT JOIN system.columns b ON a.name = b.parent
WHERE b.field_type = 2
ORDER BY tableName, b.field_num

-- NOT NULL
-- default value
-- primary key
postgres syntax:
CREATE TABLE example (
a integer,
b integer,
c integer,
PRIMARY KEY (a, c));

SELECT a.name, a.table_primary_Key, b.index_expression
FROM system.tables a
LEFT JOIN system.indexes b on a.name = b.parent
  AND a.table_primary_key = b.name
WHERE a.table_primary_key IS NOT NULL 

-- CURSOR
SELECT b.index_expression
FROM system.tables a
LEFT JOIN system.indexes b on a.name = b.parent
  AND a.table_primary_key = b.name
WHERE a.name = 'departments'

SELECT * FROM system.indexes

SELECT left(parent,20), left(name, 50), field_num, field_type, field_Length,
  field_decimal, field_can_be_null,
  field_default_value
FROM system.columns
WHERE field_type IN (17)
WHERE parent = 'callcopyrecordings'; 

SELECT *
FROM system.indexes
WHERE name = 'pk'

SELECT DISTINCT field_type FROM system.columns;
*/
/*
dds
  postgres chokes on field names that include #
    replace # with '', same fix for primary key
    ruh roh, pdpphdr has ptauth AND ptauth# resulting IN 2 ptauth BOOM
    replace # with _
  shit load of numeric(8,15):
    change them to 12,4
  TABLE xfmPayCheck, tmp401k: shit load of fields with spaces IN name, exlude for now
  TABLE tmpRoKeys, todayRoKeys: default 12/31/9999 00:00:00 bombs IN postgres
    surrounding with ' works 
    text type default values also need to be delimited 
tool (dpsVSeries)
  exclude bodyshop tables    
	
3/15/15: change timestamp to ts with timezone	
6/24/15: change text to citext

DROP TABLE #test
*/

DECLARE @str memo;
DECLARE @crlf string;
DECLARE @tableName string;
DECLARE @columnName string; 
DECLARE @lastField integer;
DECLARE @i integer;
DECLARE @schema string;
DECLARE @hasPk string;
DECLARE @tableCur cursor AS 
  SELECT LEFT(name, 50) AS tableName,
    CASE WHEN table_primary_key IS NULL THEN 'false' ELSE 'true' END AS hasPk
  FROM system.tables
  -- SET the TABLE name -------------------------------------------------------
  WHERE name = 'factorycertifications';
DECLARE @columnCur CURSOR AS 
  SELECT name, field_num, 
    CASE field_type
  	  WHEN 1 THEN 'boolean' --logical
-- simulate bigint IN ads with numeric(20,0)      
      WHEN 2 THEN -- numeric
        CASE 
          WHEN field_length = 20 AND field_decimal = 0 THEN 'bigint'
          ELSE 'numeric (' + trim(CAST(field_length AS sql_char)) 
            + ',' + trim(CAST(field_decimal as sql_char)) + ')'
        END 
  	  WHEN 3 THEN  'date' -- date
  	  WHEN 4 THEN 'citext' -- char
  	  WHEN 5 THEN 'citext' -- memo  
      WHEN 6 THEN 'citext' -- blob any -- for now, NOT sure what will WORK
      WHEN 7 THEN 'citext' -- blob bitmap -- for now, NOT sure what will WORK
      WHEN 8 THEN 'citext' -- varchar 
      WHEN 10 THEN -- double -- IN ads, default double results IN (8,15) 
        CASE
          WHEN field_length = 8 AND field_decimal = 15 THEN 'numeric (12,4)'
          ELSE 'numeric (' + trim(CAST(field_length AS sql_char)) 
          + ',' + trim(CAST(field_decimal as sql_char)) + ')'  
        END
  	  WHEN 11 THEN 'integer' -- integer
      WHEN 12 THEN 'smallint' -- shortint signed short integer
      WHEN 13 THEN 'time without time zone' -- time
  	  WHEN 14 THEN 'timestamp with time zone' -- timestamp
      WHEN 15 THEN 'serial' -- autoinc   ** TRY serial, don't know how this will WORK **
      WHEN 17 THEN 'numeric(12,4)' -- curdouble
      WHEN 18 THEN 'numeric(12,4)' -- money           
  	  WHEN 20 THEN 'citext' -- cichar
      WHEN 22 THEN 'timestamp with time zone' -- modtime
      WHEN 23 THEN 'citext' -- Visual FoxPro varchar field
  	  ELSE 'xxxx'
	  END AS field_type,
    field_type as adsFieldType,
	  field_Length,
    field_decimal, field_can_be_null,
    field_default_value
--    CASE WHEN field_default_value IS NULL THEN 'false' ELSE 'true' END AS hasDefaultValue
  FROM system.columns
  WHERE parent = @tableName; 
DECLARE @lastFieldCur CURSOR AS 
  SELECT MAX(field_num) AS lastField
  FROM system.columns
  WHERE parent = @tableName;   
DECLARE @pkCur CURSOR AS 
  SELECT b.index_expression
  FROM system.tables a
  LEFT JOIN system.indexes b on a.name = b.parent
    AND a.table_primary_key = b.name
  WHERE a.name = @tableName;  
@crlf = Char(13) + Char(10);  
@str = '';  
-- SET the schema -------------------------------------------------------------
@schema = 'ads';
OPEN @tableCur; 
TRY
  WHILE FETCH @tableCur DO
    @tableName = @tableCur.tableName;
    @hasPk = @tableCur.hasPk;
    @i = 1;
    OPEN @lastFieldCur;
    TRY
      WHILE FETCH @lastFieldCur DO 
        @lastField = @lastFieldCur.lastField;
      END WHILE;
    FINALLY 
      CLOSE @lastFieldCur;
    END TRY;
	  -- @str = @str + 'create TABLE ' + @schema + '.sco_' + @tableName + '(' + @crlf;
    @str = @str + 'create TABLE ' + @schema + '.' + @tableName + '(' + @crlf; 
  	OPEN @columnCur;
  	TRY
  	  WHILE FETCH @columnCur DO
        @columnName = replace(@columnCur.name, '#', '_');
        IF @lastField = 1 THEN -- single COLUMN tables
          @str = @str + '    ' +  @columnName + ' ' +  @columnCur.field_type 
            + iif(@columnCur.field_can_be_null = true, '',' NOT NULL')
            + iif(CAST(@columnCur.field_default_value AS sql_char) IS NULL,'',' DEFAULT ' 
            + iif(@columnCur.adsFieldType IN (3,4,5,6,7,8,13,14,20,22,23), '''' + @columnCur.field_default_value + '''', @columnCur.field_default_value));
          IF @hasPk = 'false' THEN 
            @str = @str + ')' + @crlf;
          ELSE -- has pk
            OPEN @pkCur;
            TRY 
              WHILE FETCH @pkCur DO 
                @str = @str + ',' + @crlf + 'PRIMARY KEY (' + replace(@pkCur.index_expression,';',',') + '))';
              END WHILE;
            FINALLY
              CLOSE @pkCur;
            END TRY;
          END IF; -- has pk  
        ELSEIF @i < @lastField THEN  -- not the last field, END line with comma    
  		    @str = @str + '    ' + @columnName + ' ' +  @columnCur.field_type	
            + iif(@columnCur.field_can_be_null = true, '',' NOT NULL') 
            + iif(CAST(@columnCur.field_default_value AS sql_char) IS NULL,'',' DEFAULT ' 
            + iif(@columnCur.adsFieldType IN (3,4,5,6,7,8,13,14,20,22,23), '''' + @columnCur.field_default_value + '''', @columnCur.field_default_value)) 
            + ',' + @crlf;
          @i = @i + 1;
        ELSE -- this is the last field, END line with right paren
          IF @hasPk = 'false' THEN 
            @str = @str + '    ' +  @columnName + ' ' +  @columnCur.field_type	
            + iif(CAST(@columnCur.field_default_value AS sql_char) IS NULL,'',' DEFAULT ' 
            + iif(@columnCur.adsFieldType IN (3,4,5,6,7,8,13,14,20,22,23), '''' + @columnCur.field_default_value + '''', @columnCur.field_default_value)) 
            + ')' + @crlf;
          ELSE -- has pk
            OPEN @pkCur;
            TRY 
              WHILE FETCH @pkCur DO 
                @str = @str + '    ' +  @columnName + ' ' +  @columnCur.field_type	
                + iif(CAST(@columnCur.field_default_value AS sql_char) IS NULL,'',' DEFAULT ' 
                + iif(@columnCur.adsFieldType IN (3,4,5,6,7,8,13,14,20,22,23), '''' + @columnCur.field_default_value + '''', @columnCur.field_default_value)) 
                + ',' + @crlf;              
                @str = @str + '    ' + 'PRIMARY KEY (' + replace(replace(@pkCur.index_expression,';',','), '#', '_') + '))';
              END WHILE;
            FINALLY
              CLOSE @pkCur;
            END TRY;
          END IF; -- has pk
        END IF; -- last field
  	  END WHILE; -- WHILE FETCH @columnCur DO
  	FINALLY
  	  CLOSE @columnCur;
  	END TRY;
    @str = @str + ' WITH (OIDS=FALSE);' + @crlf + @crlf;
  END WHILE; --FETCH @tableCur DO
FINALLY
  CLOSE @tableCur;
END TRY;  


SELECT @str 
INTO #test
FROM system.iota;

