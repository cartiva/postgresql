﻿select to_date('5/31/2014 6:43 AM', 'Mon DD YYYY')

select '5/31/2014 6:43 AM'::timestamp

copy stgDigitalAirStrike (
          DealerName, VIN, RONumber, SurveySentTS,
	  TransactionType, CustomerName, CustomerEmail, UnsubscribeTS, SurveyResponseTS, ConsentToPublish, StarRating,
	  ReferralLikelihood, CustomerExperience, CustomerSuggestion, SalesPersonName)
from 'C:\xfrData\test1.csv'
delimiter ',' CSV 


-- Table: stgdigitalairstrike

-- DROP TABLE stgdigitalairstrike;

CREATE TABLE stgdigitalairstrike
(
  dealername text,
  vin text,
  ronumber text,
  surveysentts text,
  transactiontype text,
  customername text,
  customeremail text,
  unsubscribets text,
  surveyresponsets text,
  consenttopublish text,
  starrating text,
  referrallikelihood text,
  customerexperience text,
  passprofanity text,
  dealerresponsets text,
  dealerresponse text,
  salespersonname text,
  publicationdate date,
  publicationstatus text,
  customersuggestion text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE stgdigitalairstrike
  OWNER TO postgres;


 select * from stgDigitalAirStrike where starrating is not null