﻿-- each day change the date portion of table names and run the entire commented section
/*
-- Table: test.ext_glptrns_1203

-- DROP TABLE test.ext_glptrns_1203;

CREATE TABLE test.ext_glptrns_1203
(
  gtco_ citext, -- GTCO# : Company Number
  gttrn_ bigint, -- GTTRN# : Trans Number
  gtseq_ integer, -- GTSEQ# : Tran Seq Number
  gtdtyp citext, -- GTDTYP : Doc Type
  gttype citext, -- GTTYPE : Record Type
  gtpost citext, -- GTPOST : Post Status
  gtrsts citext, -- GTRSTS : Reconcile Status
  gtadjust citext, -- GTADJUST : Adjustment Tran
  gtpsel citext, -- GTPSEL : Select to Pay
  gtjrnl citext, -- GTJRNL : Journal
  gtdate date, -- GTDATE : Transaction Date
  gtrdate date, -- GTRDATE : Reconciled Date
  gtsdate date, -- GTSDATE : A/R Statemnt Date
  gtacct citext, -- GTACCT : Account Number
  gtctl_ citext, -- GTCTL# : Control Number
  gtdoc_ citext, -- GTDOC# : Document Number
  gtrdoc_ citext, -- GTRDOC# : Reconciled Doc#
  gtrdtyp citext, -- GTRDTYP : Recon Doc Type
  gtodoc_ citext, -- GTODOC# : Outside Doc#
  gtref_ citext, -- GTREF# : Reference Number
  gtvnd_ citext, -- GTVND# : Vendor Number
  gtdesc citext, -- GTDESC : Description
  gttamt numeric(11,2), -- GTTAMT : Transaction Amt
  gtcost numeric(11,2), -- GTCOST : Transaction Cost
  gtctlo citext, -- GTCTLO : Control Overide
  gtoco_ citext -- GTOCO# : Originating Co#
)
WITH (
  OIDS=FALSE
);
ALTER TABLE test.ext_glptrns_1203
  OWNER TO postgres;
COMMENT ON COLUMN test.ext_glptrns_1203.gtco_ IS 'GTCO# : Company Number';
COMMENT ON COLUMN test.ext_glptrns_1203.gttrn_ IS 'GTTRN# : Trans Number';
COMMENT ON COLUMN test.ext_glptrns_1203.gtseq_ IS 'GTSEQ# : Tran Seq Number';
COMMENT ON COLUMN test.ext_glptrns_1203.gtdtyp IS 'GTDTYP : Doc Type';
COMMENT ON COLUMN test.ext_glptrns_1203.gttype IS 'GTTYPE : Record Type';
COMMENT ON COLUMN test.ext_glptrns_1203.gtpost IS 'GTPOST : Post Status';
COMMENT ON COLUMN test.ext_glptrns_1203.gtrsts IS 'GTRSTS : Reconcile Status';
COMMENT ON COLUMN test.ext_glptrns_1203.gtadjust IS 'GTADJUST : Adjustment Tran';
COMMENT ON COLUMN test.ext_glptrns_1203.gtpsel IS 'GTPSEL : Select to Pay';
COMMENT ON COLUMN test.ext_glptrns_1203.gtjrnl IS 'GTJRNL : Journal';
COMMENT ON COLUMN test.ext_glptrns_1203.gtdate IS 'GTDATE : Transaction Date';
COMMENT ON COLUMN test.ext_glptrns_1203.gtrdate IS 'GTRDATE : Reconciled Date';
COMMENT ON COLUMN test.ext_glptrns_1203.gtsdate IS 'GTSDATE : A/R Statemnt Date';
COMMENT ON COLUMN test.ext_glptrns_1203.gtacct IS 'GTACCT : Account Number';
COMMENT ON COLUMN test.ext_glptrns_1203.gtctl_ IS 'GTCTL# : Control Number';
COMMENT ON COLUMN test.ext_glptrns_1203.gtdoc_ IS 'GTDOC# : Document Number';
COMMENT ON COLUMN test.ext_glptrns_1203.gtrdoc_ IS 'GTRDOC# : Reconciled Doc#';
COMMENT ON COLUMN test.ext_glptrns_1203.gtrdtyp IS 'GTRDTYP : Recon Doc Type';
COMMENT ON COLUMN test.ext_glptrns_1203.gtodoc_ IS 'GTODOC# : Outside Doc#';
COMMENT ON COLUMN test.ext_glptrns_1203.gtref_ IS 'GTREF# : Reference Number';
COMMENT ON COLUMN test.ext_glptrns_1203.gtvnd_ IS 'GTVND# : Vendor Number';
COMMENT ON COLUMN test.ext_glptrns_1203.gtdesc IS 'GTDESC : Description';
COMMENT ON COLUMN test.ext_glptrns_1203.gttamt IS 'GTTAMT : Transaction Amt';
COMMENT ON COLUMN test.ext_glptrns_1203.gtcost IS 'GTCOST : Transaction Cost';
COMMENT ON COLUMN test.ext_glptrns_1203.gtctlo IS 'GTCTLO : Control Overide';
COMMENT ON COLUMN test.ext_glptrns_1203.gtoco_ IS 'GTOCO# : Originating Co#';

CREATE TABLE test.stg_glptrns_1203
(
  gtco_ citext, -- GTCO# : Company Number
  gttrn_ bigint, -- GTTRN# : Trans Number
  gtseq_ integer, -- GTSEQ# : Tran Seq Number
  gtdtyp citext, -- GTDTYP : Doc Type
  gttype citext, -- GTTYPE : Record Type
  gtpost citext, -- GTPOST : Post Status
  gtrsts citext, -- GTRSTS : Reconcile Status
  gtadjust citext, -- GTADJUST : Adjustment Tran
  gtpsel citext, -- GTPSEL : Select to Pay
  gtjrnl citext, -- GTJRNL : Journal
  gtdate date, -- GTDATE : Transaction Date
  gtrdate date, -- GTRDATE : Reconciled Date
  gtsdate date, -- GTSDATE : A/R Statemnt Date
  gtacct citext, -- GTACCT : Account Number
  gtctl_ citext, -- GTCTL# : Control Number
  gtdoc_ citext, -- GTDOC# : Document Number
  gtrdoc_ citext, -- GTRDOC# : Reconciled Doc#
  gtrdtyp citext, -- GTRDTYP : Recon Doc Type
  gtodoc_ citext, -- GTODOC# : Outside Doc#
  gtref_ citext, -- GTREF# : Reference Number
  gtvnd_ citext, -- GTVND# : Vendor Number
  gtdesc citext, -- GTDESC : Description
  gttamt numeric(11,2), -- GTTAMT : Transaction Amt
  gtcost numeric(11,2), -- GTCOST : Transaction Cost
  gtctlo citext, -- GTCTLO : Control Overide
  gtoco_ citext, -- GTOCO# : Originating Co#

-- code_title PRIMARY KEY(code,title)  
  constraint stg_glptrns_1203_pk primary key (gtco_, gttrn_, gtseq_)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE test.stg_glptrns_1203
  OWNER TO postgres;
COMMENT ON COLUMN test.stg_glptrns_1203.gtco_ IS 'GTCO# : Company Number';
COMMENT ON COLUMN test.stg_glptrns_1203.gttrn_ IS 'GTTRN# : Trans Number';
COMMENT ON COLUMN test.stg_glptrns_1203.gtseq_ IS 'GTSEQ# : Tran Seq Number';
COMMENT ON COLUMN test.stg_glptrns_1203.gtdtyp IS 'GTDTYP : Doc Type';
COMMENT ON COLUMN test.stg_glptrns_1203.gttype IS 'GTTYPE : Record Type';
COMMENT ON COLUMN test.stg_glptrns_1203.gtpost IS 'GTPOST : Post Status';
COMMENT ON COLUMN test.stg_glptrns_1203.gtrsts IS 'GTRSTS : Reconcile Status';
COMMENT ON COLUMN test.stg_glptrns_1203.gtadjust IS 'GTADJUST : Adjustment Tran';
COMMENT ON COLUMN test.stg_glptrns_1203.gtpsel IS 'GTPSEL : Select to Pay';
COMMENT ON COLUMN test.stg_glptrns_1203.gtjrnl IS 'GTJRNL : Journal';
COMMENT ON COLUMN test.stg_glptrns_1203.gtdate IS 'GTDATE : Transaction Date';
COMMENT ON COLUMN test.stg_glptrns_1203.gtrdate IS 'GTRDATE : Reconciled Date';
COMMENT ON COLUMN test.stg_glptrns_1203.gtsdate IS 'GTSDATE : A/R Statemnt Date';
COMMENT ON COLUMN test.stg_glptrns_1203.gtacct IS 'GTACCT : Account Number';
COMMENT ON COLUMN test.stg_glptrns_1203.gtctl_ IS 'GTCTL# : Control Number';
COMMENT ON COLUMN test.stg_glptrns_1203.gtdoc_ IS 'GTDOC# : Document Number';
COMMENT ON COLUMN test.stg_glptrns_1203.gtrdoc_ IS 'GTRDOC# : Reconciled Doc#';
COMMENT ON COLUMN test.stg_glptrns_1203.gtrdtyp IS 'GTRDTYP : Recon Doc Type';
COMMENT ON COLUMN test.stg_glptrns_1203.gtodoc_ IS 'GTODOC# : Outside Doc#';
COMMENT ON COLUMN test.stg_glptrns_1203.gtref_ IS 'GTREF# : Reference Number';
COMMENT ON COLUMN test.stg_glptrns_1203.gtvnd_ IS 'GTVND# : Vendor Number';
COMMENT ON COLUMN test.stg_glptrns_1203.gtdesc IS 'GTDESC : Description';
COMMENT ON COLUMN test.stg_glptrns_1203.gttamt IS 'GTTAMT : Transaction Amt';
COMMENT ON COLUMN test.stg_glptrns_1203.gtcost IS 'GTCOST : Transaction Cost';
COMMENT ON COLUMN test.stg_glptrns_1203.gtctlo IS 'GTCTLO : Control Overide';
COMMENT ON COLUMN test.stg_glptrns_1203.gtoco_ IS 'GTOCO# : Originating Co#';

*/

-- and then after populating ext_glptrns_mmdd, this
insert into test.stg_glptrns_1203
select a.*
from test.ext_glptrns_1203 a
left join (
  select gtco_, gttrn_, gtseq_
  from test.ext_glptrns_1203
  group by gtco_,gttrn_, gtseq_
  having count(*) > 1) b on a.gtco_ = b.gtco_
    and a.gttrn_ = b.gttrn_
    and a.gtseq_ = b.gtseq_
where a.gtpost <> 'V'
  AND a.gttrn_ > 0
  and b.gtco_ is null 

select *
from (
  select gtco_, gttrn_, gtseq_, md5(stg_glptrns_1127::text)
  from test.stg_glptrns_1127) a
inner join (
  select gtco_, gttrn_, gtseq_, md5(stg_glptrns_1203::text)
  from test.stg_glptrns_1203) b on a.gtco_ = b.gtco_ 
    and a.gttrn_ = b.gttrn_ 
    and a.gtseq_ = b.gtseq_
    and a.md5 <> b.md5


select '1127', r.*
from test.stg_glptrns_1127 r
inner join (
  select a.gtco_, a.gttrn_, a.gtseq_
  from (
    select gtco_, gttrn_, gtseq_, md5(stg_glptrns_1127::text)
    from test.stg_glptrns_1127) a
  inner join (
    select gtco_, gttrn_, gtseq_, md5(stg_glptrns_1203::text)
    from test.stg_glptrns_1203) b on a.gtco_ = b.gtco_ 
      and a.gttrn_ = b.gttrn_ 
      and a.gtseq_ = b.gtseq_
      and a.md5 <> b.md5) s on r.gtco_ = s.gtco_ and r.gttrn_ = s.gttrn_ and r.gtseq_ = s.gtseq_
union
select '1203', r.*
from test.stg_glptrns_1203 r
inner join (
  select a.gtco_, a.gttrn_, a.gtseq_
  from (
    select gtco_, gttrn_, gtseq_, md5(stg_glptrns_1127::text)
    from test.stg_glptrns_1127) a
  inner join (
    select gtco_, gttrn_, gtseq_, md5(stg_glptrns_1203::text)
    from test.stg_glptrns_1203) b on a.gtco_ = b.gtco_ 
      and a.gttrn_ = b.gttrn_ 
      and a.gtseq_ = b.gtseq_
      and a.md5 <> b.md5) s on r.gtco_ = s.gtco_ and r.gttrn_ = s.gttrn_ and r.gtseq_ = s.gtseq_      
order by gtco_, gttrn_, gtseq_  


-- 12/23/15
ext_comparisons
assume gtco_, gttrn_, gtseq_ as a "durable" natural key

-- dups?
select gtco_, gttrn_, gtseq_
from test.ext_glptrns_1127
group by gtco_, gttrn_, gtseq_
having count(*) > 1

-- for this limited subset, the only duplicates are all voids
-- so, before this date, did trn 3121718 seq 1 have a non void entry?
select *
from test.ext_glptrns_1127 a
inner join (
  select gtco_, gttrn_, gtseq_
  from test.ext_glptrns_1127
  group by gtco_, gttrn_, gtseq_
  having count(*) > 1) b on a.gtco_ = b.gtco_
    and a.gttrn_ = b.gttrn_
    and a.gtseq_ = b.gtseq_
order by a.gtco_, a.gttrn_, a.gtseq_    

select * -- 8 sec
from (
  select gtco_, gttrn_, gtseq_, md5(ext_glptrns_1127::text) as hash 
  from test.ext_glptrns_1127) a
inner join (
select gtco_, gttrn_, gtseq_, md5(ext_glptrns_1205::text) as hash
from test.ext_glptrns_1205) b on a.gtco_ = b.gtco_ 
  and a.gttrn_ = b.gttrn_
  and a.gtseq_ = b.gtseq_
  and a.hash <> b.hash


select a.* -- 7.5 seci
from (
  select gtco_, gttrn_, gtseq_, md5(ext_glptrns_1127::text) as hash 
  from test.ext_glptrns_1127) a
where exists (
  select 1
  from test.ext_glptrns_1205 
  WHERE gtco_ = a.gtco_ 
    and gttrn_ = a.gttrn_
    and gtseq_ = a.gtseq_
    and md5(ext_glptrns_1205::text) <> a.hash)


-- 1127 & 1222

select '1127' as the_date, m.*
from test.ext_glptrns_1127 m
inner join (
  select a.* 
  from (
    select gtco_, gttrn_, gtseq_, md5(ext_glptrns_1127::text) as hash 
    from test.ext_glptrns_1127) a
  where exists (
    select 1
    from test.ext_glptrns_1222 
    WHERE gtco_ = a.gtco_ 
      and gttrn_ = a.gttrn_
      and gtseq_ = a.gtseq_
      and md5(ext_glptrns_1222::text) <> a.hash)) n on m.gtco_ = n.gtco_ 
        and m.gttrn_ = n.gttrn_
        and m.gtseq_ = n.gtseq_  
union
select '1222' as the_date, m.*
from test.ext_glptrns_1222 m
inner join (
  select a.* 
  from (
    select gtco_, gttrn_, gtseq_, md5(ext_glptrns_1127::text) as hash 
    from test.ext_glptrns_1127) a
  where exists (
    select 1
    from test.ext_glptrns_1222 
    WHERE gtco_ = a.gtco_ 
      and gttrn_ = a.gttrn_
      and gtseq_ = a.gtseq_
      and md5(ext_glptrns_1222::text) <> a.hash)) n on m.gtco_ = n.gtco_ 
        and m.gttrn_ = n.gttrn_
        and m.gtseq_ = n.gtseq_   
order by gtco_, gttrn_, gtseq_, the_date

-- min date in 0702
-- select min(gtdate) from test.ext_glptrns_0702
-- 05/19 and 07/02
select '0519' as the_date, m.*
from test.ext_glptrns_0519 m
inner join (
  select a.* 
  from (
    select gtco_, gttrn_, gtseq_, md5(ext_glptrns_0519::text) as hash 
    from test.ext_glptrns_0519) a
  where exists (
    select 1
    from test.ext_glptrns_0702 
    WHERE gtco_ = a.gtco_ 
      and gttrn_ = a.gttrn_
      and gtseq_ = a.gtseq_
      and md5(ext_glptrns_0702::text) <> a.hash)) n on m.gtco_ = n.gtco_ 
        and m.gttrn_ = n.gttrn_
        and m.gtseq_ = n.gtseq_  
union
select '0702' as the_date, m.*
from test.ext_glptrns_0702 m
inner join (
  select a.* 
  from (
    select gtco_, gttrn_, gtseq_, md5(ext_glptrns_0519::text) as hash 
    from test.ext_glptrns_0519) a
  where exists (
    select 1
    from test.ext_glptrns_0702
    WHERE gtco_ = a.gtco_ 
      and gttrn_ = a.gttrn_
      and gtseq_ = a.gtseq_
      and md5(ext_glptrns_0702::text) <> a.hash)) n on m.gtco_ = n.gtco_ 
        and m.gttrn_ = n.gttrn_
        and m.gtseq_ = n.gtseq_   
order by gtco_, gttrn_, gtseq_, the_date

drop table if exists _0519;
create temp table _0519 as
select gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtjrnl, 
  gtdate, gtacct, gtctl_, gtdoc_, gtref_, gtdesc, gttamt, rrn
from test.ext_glptrns_0519  

drop table if exists _0702;
create temp table _0702 as
select gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtjrnl, 
  gtdate, gtacct, gtctl_, gtdoc_, gtref_, gtdesc, gttamt, rrn
from test.ext_glptrns_0702  

select *
from _0702 a
inner join (
  select gttrn_, gtseq_ from _0702 group by gttrn_, gtseq_ having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_
order by a.gttrn_, a.gtseq_  

select gttrn_, gtseq_ 
from _0702 a
where gtpost = 'Y'
group by gttrn_, gtseq_ 
having count(*) > 1


drop table if exists _0705;
create temp table _0705 as
select gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtjrnl, 
  gtdate, gtacct, gtctl_, gtdoc_, gtref_, gtdesc, gttamt, rrn
from test.ext_glptrns_0705  

select * from _0705 where gtacct like '2%'limit 1000

drop table if exists _0522;
create temp table _0522 as
select gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtjrnl, 
  gtdate, gtacct, gtctl_, gtdoc_, gtref_, gtdesc, gttamt, rrn
from test.ext_glptrns_0522 

create temp table voids as
select *
from _0705 a
where exists (
  select 1
  from _0705
  where gtpost = 'V'
    and gttrn_ = a.gttrn_
    and gtseq_ = a.gtseq_)
order by gttrn_, gtseq_    

select * 
from voids a
where exists (
  select 1
  from voids
  where gtpost = 'Y'
    and gttrn_ = a.gttrn_
    and gtseq_ = a.gtseq_)
order by gttrn_, gtseq_    

select rrn from _0705 group by rrn having count(*) > 1

select *
from voids
where gtdesc not like 'NO LA%'    
order by gttrn_, gtseq_    



-- ok, now, do any of this subset of attributes change over time?

-- what this alledges is that in the subset of attributes in the _0519 & _0702 tables
-- excluding void transactions, no values change over time
select '0519' as the_date, m.*
from _0519 m
inner join (
  select a.* 
  from (
    select gtco_, gttrn_, gtseq_, md5(_0519::text) as hash 
    from _0519
    where gtpost = 'Y') a
  where exists (
    select 1
    from _0702 
    where gtpost = 'Y'
      AND gtco_ = a.gtco_ 
      and gttrn_ = a.gttrn_
      and gtseq_ = a.gtseq_
      and md5(_0702::text) <> a.hash)) n on m.gtco_ = n.gtco_ 
        and m.gttrn_ = n.gttrn_
        and m.gtseq_ = n.gtseq_  
union
select '0702' as the_date, m.*
from _0702 m
inner join (
  select a.* 
  from (
    select gtco_, gttrn_, gtseq_, md5(_0702::text) as hash 
    from _0702
    where gtpost = 'Y') a
  where exists (
    select 1
    from _0519
    where gtpost = 'Y'
      and gtco_ = a.gtco_ 
      and gttrn_ = a.gttrn_
      and gtseq_ = a.gtseq_
      and md5(_0519::text) <> a.hash)) n on m.gtco_ = n.gtco_ 
        and m.gttrn_ = n.gttrn_
        and m.gtseq_ = n.gtseq_   
order by gtco_, gttrn_, gtseq_, the_date

CREATE UNIQUE INDEX ON _0702 (gttrn_, gtseq_) WHERE (gtpost = 'Y');

CREATE UNIQUE INDEX ON test.ext_glptrns_0702 (gttrn_, gtseq_) WHERE (gtpost = 'Y');

select rrn from test.ext_glptrns_0702 group by rrn having count(*) > 1

select * from _0702 limit 1000


--- glpmast --------------------------------------------------------------------------------------------------------------------------
the question is how to deal with changed rows
it appears that we re use account numbers

1403006 was SLS N/C CONVERSIONS
        now is SLS N/C CADILLAC CT6

1427004 was SLS N/T RAINIER
        now is SLS N/T BUCK ENVISION     
          
-- year/account_number::unique
select year, account_number
from (
select company_number, fiscal_annual, year, account_number, account_type,
  account_type, account_desc, account_sub_type, 
  department, typical_balance, active 
from test.ext_glpmast_0702 
where account_number is not null
) x group by year, account_number having count(*) > 1


drop table if exists _0702;
create temp table _0702 as
select company_number, fiscal_annual, year, account_number, 
  account_type, account_desc, account_sub_type, 
  department, typical_balance, active 
from test.ext_glpmast_0702 
where account_number is not null
  and year = 2016;
create unique index on _0702(account_number);  

drop table if exists _0115;
create temp table _0115 as
select company_number, fiscal_annual, year, account_number, 
  account_type, account_desc, account_sub_type, 
  department, typical_balance, active 
from test.ext_glpmast_0115 
where account_number is not null
  and year = 2016;
create unique index on _0115(account_number);  

-- yep, there are a few that change over time
select '0115' as the_date, m.*
from _0115 m
inner join (
  select a.*
  from (
    select account_number, md5(_0115::text) as hash
    from _0115) a
  where exists (
    select 1
    from _0702
    where account_number = a.account_number 
      and md5(_0702::Text) <> a.hash)) n on m.account_number = n.account_number    
union
select '0702' as the_date, m.*
from _0702 m
inner join (
  select a.*
  from (
    select account_number, md5(_0702::text) as hash
    from _0702) a
  where exists (
    select 1
    from _0115
    where account_number = a.account_number
      and md5(_0115::Text) <> a.hash)) n on m.account_number = n.account_number    
order by account_number, the_date

specifically, changes exposed:
  department changes: acct 12224, 15104M
  description changes: 12304B, 1403006, 1603006, 23301
  an inactive acct becomes active with a new description: 1427004,  1627004
  an active acct becomes inactive: 19054, 19084

-- the above query does not expose new rows, but these do
-- i am thinking an account never gets deleted from glpmast, just made inactive

select * from _0115 a where not exists (select 1 from _0702 where account_number = a.account_number)
select * from _0702 a where not exists (select 1 from _0115 where account_number = a.account_number)

-- rows that have changed and rows that have been deleted
(select * from _0115 a
except
select * from _0702) 
union all
-- rows that have changed and new rows
(select * from _0702
except
select * from _0115)
order by account_number


-- ffpxrefdta --------------------------------------------------------------------------------------------------------------  
-- gl_acct unique with the included limits
select g_l_acct_number 
-- select *
from test.ext_ffpxrefdta_0702 
where factory_financial_year = 2016
  and g_l_acct_number is not null
  and coalesce(consolidation_grp, '1') <> '3'
  and factory_code = 'GM'
  and factory_account <> '331A'
group by g_l_acct_number
having count(*) > 1

drop table if exists _0120;
create temp table _0120 as 
select coalesce(consolidation_grp, '1'), g_l_acct_number, factory_account
from test.ext_ffpxrefdta_0120
where factory_financial_year = 2016
  and g_l_acct_number is not null
  and coalesce(consolidation_grp, '1') <> '3'
  and factory_code = 'GM'
  and factory_account <> '331A';
create unique index on _0120(g_l_acct_number);

drop table if exists _0702;
create temp table _0702 as 
select coalesce(consolidation_grp, '1'), g_l_acct_number, factory_account
from test.ext_ffpxrefdta_0702
where factory_financial_year = 2016
  and g_l_acct_number is not null
  and coalesce(consolidation_grp, '1') <> '3'
  and factory_code = 'GM'
  and factory_account <> '331A';
create unique index on _0702(g_l_acct_number);  

-- no apparent changes over time
select '0120' as the_date, m.*
from _0120 m
inner join (
  select a.*
  from (
    select g_l_acct_number, md5(_0120::text) as hash
    from _0120) a
  where exists (
    select 1
    from _0702
    where g_l_acct_number = a.g_l_acct_number 
      and md5(_0702::Text) <> a.hash)) n on m.g_l_acct_number = n.g_l_acct_number    
union
select '0702' as the_date, m.*
from _0702 m
inner join (
  select a.*
  from (
    select g_l_acct_number, md5(_0702::text) as hash
    from _0702) a
  where exists (
    select 1
    from _0120
    where g_l_acct_number = a.g_l_acct_number
      and md5(_0120::Text) <> a.hash)) n on m.g_l_acct_number = n.g_l_acct_number          
order by g_l_acct_number, the_date;

(select * from _0120
except
select * from _0702) 
union 
(select * from _0702
except
select * from _0120)

-- sypffxmst  ------------------------------------------------------------------------------------------------------
select * 
from test.ext_sypffxmst_0702
where fxmcyy = 2016
order by fxmpge,fxmlne,fxmcol
-- sweet, gm acct is unique
select fxmact
from test.ext_sypffxmst_0702
where fxmcyy = 2016
group by fxmact
having count(*) > 1

drop table if exists _0120 cascade;
create temp table _0120 as 
select fxmcyy,fxmact,fxmpge,fxmlne,fxmcol,fxmstr
from test.ext_sypffxmst_0120
where fxmcyy = 2016;
create unique index on _0120(fxmact);

drop table if exists _0702 cascade;
create temp table _0702 as 
select fxmcyy,fxmact,fxmpge,fxmlne,fxmcol,fxmstr
from test.ext_sypffxmst_0702
where fxmcyy = 2016;
create unique index on _0702(fxmact);

-- no apparent changes over time
select '0120' as the_date, m.*
from _0120 m
inner join (
  select a.*
  from (
    select fxmact, md5(_0120::text) as hash
    from _0120) a
  where exists (
    select 1
    from _0702
    where fxmact = a.fxmact 
      and md5(_0702::Text) <> a.hash)) n on m.fxmact = n.fxmact    
union
select '0702' as the_date, m.*
from _0702 m
inner join (
  select a.*
  from (
    select fxmact, md5(_0702::text) as hash
    from _0702) a
  where exists (
    select 1
    from _0120
    where fxmact = a.fxmact
      and md5(_0120::Text) <> a.hash)) n on m.fxmact = n.fxmact          
order by fxmact, the_date;


-- 7/6 -- some more glptrns stuff --------------------------------------------------------------------------

drop table if exists _0705;
create temp table _0705 as
select gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtjrnl, 
  gtdate, gtacct, gtctl_, gtdoc_, gtref_, gtdesc, gttamt, rrn
from test.ext_glptrns_0705  

drop table if exists _0522;
create temp table _0522 as
select gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtjrnl, 
  gtdate, gtacct, gtctl_, gtdoc_, gtref_, gtdesc, gttamt, rrn
from test.ext_glptrns_0522 

-- rrn does not change over time
select * 
from _0705 a
inner join _0522 b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_
  and b.gtpost = 'Y'
where a.gtpost = 'Y'
  and a.rrn <> b.rrn



drop table if exists _0630;
create temp table _0630 as
select gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtjrnl, 
  gtdate, gtacct, gtctl_, gtdoc_, gtref_, gtdesc, gttamt, rrn
from test.ext_glptrns_0630;  

drop table if exists _0701;
create temp table _0701 as
select gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtjrnl, 
  gtdate, gtacct, gtctl_, gtdoc_, gtref_, gtdesc, gttamt, rrn
from test.ext_glptrns_0701; 

-- identical results (10915 rows) whether rrn or trn/seq
select *
from _0701 a
where not exists (
  select 1
  from _0630
  where rrn = a.rrn)

select *
from _0701 a
where not exists (
  select 1
  from _0630
  where gttrn_ = a.gttrn_
    and gtseq_ = a.gtseq_)  

-- mind fucking on voids
-- sometimes there are 2 trn/seq that are identical (except for rrn)
-- sometimes (when one is not void) the 2 trn/seq rows are different
select *
from _0702 a
inner join (
  select gttrn_, gtseq_ from _0702 group by gttrn_, gtseq_ having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_
order by a.gttrn_, a.gtseq_  

-- the 2 rows are identical (except for rrn)
select *
from test.ext_glptrns_0702 
where gttrn_ = 3320990
  and gtseq_ = 1

-- and sometimes they are not the same  
select *
from test.ext_glptrns_0702 
where gttrn_ = 3322184
  and gtseq_ = 1
  
so do both of these rows go into the ext_glptrns no dammit

from the fact_gl script:

-- so it would appear that these are the rows that need to be added
(select gttrn_, gtseq_, gtpost
from test.ext_glptrns_0701
group by gttrn_,gtseq_,gtpost)
except
(select gttrn_, gtseq_, gtpost
from test.ext_glptrns_0630
group by gttrn_,gtseq_,gtpost)

to narrow these down and not submit double rows for when there are 2 identical (except for rrn) void rows

select * 
from voids a
inner join (
select gttrn_, gtseq_
from voids
group by gttrn_, gtseq_
having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_
order by a.gttrn_ , a.gtseq_


select * 
from voids a
left  join (
select gttrn_, gtseq_
from voids
group by gttrn_, gtseq_
having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_
where b.gttrn_ is null 
order by a.gttrn_ , a.gtseq_

-- 7/6 -- sketch it out --------------------------------------------------------------------------
-- will need to accomodate accounts *E*, *SPRD, *VOID
-- as well as blank journal and other columns since i have chosen to include funky rows 

-- -- INITIAL LOAD
drop table if exists test.ext_glptrns cascade;
CREATE TABLE test.ext_glptrns(
  gtco_ citext,
  gttrn_ bigint,
  gtseq_ integer,
  gtdtyp citext,
  gttype citext,
  gtpost citext,
  gtrsts citext,
  gtadjust citext,
  gtpsel citext,
  gtjrnl citext,
  gtdate date,
  gtrdate date,
  gtsdate date,
  gtacct citext,
  gtctl_ citext,
  gtdoc_ citext,
  gtrdoc_ citext,
  gtrdtyp citext,
  gtodoc_ citext,
  gtref_ citext,
  gtvnd_ citext,
  gtdesc citext,
  gttamt numeric(11,2),
  gtcost numeric(11,2),
  gtctlo citext,
  gtoco_ citext,
  rrn bigint unique);
create unique index on ext_glptrns(gttrn_,gtseq_) where gtpost = 'Y';
  
insert into test.ext_glptrns
select gtco_,gttrn_,gtseq_,gtdtyp,gttype,gtpost,gtrsts,gtadjust,
  gtpsel,gtjrnl,gtdate,gtrdate,gtsdate,gtacct,gtctl_,gtdoc_,
  gtrdoc_,gtrdtyp,gtodoc_,gtref_,gtvnd_,gtdesc,gttamt,gtcost,
  gtctlo,gtoco_,rrn
from test.ext_glptrns_0501;

delete 
from test.ext_glptrns
where rrn in (12664356, 12664357, 12664353, 12664355);

-- -- and the relevant attributes
-- -- select * from test.ext_glptrns limit 100;
-- -- don't need the xfm level, just insert the relevant attributes from the 
-- -- relevant subset of glptrns into fact_gl
-- drop table if exists test.xfm_glptrns cascade;
-- create table test.xfm_glptrns (
--   trans bigint not null,
--   seq integer not null,
--   doc_type_code citext not null,
--   post_status citext not null,
--   journal_code citext not null,
--   the_date date not null,
--   account citext not null,
--   control citext not null,
--   doc citext not null,
--   ref citext not null,
--   description citext not null,
--   amount numeric(12,2) not null,
--   rrn bigint unique);
-- create unique index on test.xfm_glptrns(trans,seq) where post_status = 'Y';
-- insert into test.xfm_glptrns
-- select gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, 
--   coalesce(gtacct, 'none'), 
--   gtctl_, gtdoc_, gtref_, gtdesc, gttamt, rrn
-- from test.ext_glptrns;

/*

description will be a separate dimension if it is truly needed/wanted, in accordance
with Kimball
-- 277905
select count(*) from test.xfm_glptrns
-- 30275
select count(*) from (select description from test.xfm_glptrns group by description) x

*/
-- this structure will be different in production: dimension keys
drop table if exists test.fact_gl cascade;
create table test.fact_gl (
  trans bigint not null,
  seq integer not null,
  doc_type_code citext not null, 
  post_status citext not null,
  journal_code citext not null,
  the_date date not null,
  account citext not null,
  control citext not null,
  doc citext not null,
  ref citext not null,
  amount numeric(12,2) not null,
  rrn bigint unique not null);
create unique index on test.fact_gl(trans, seq) where post_status = 'Y'; 

/*
this will be from a join on the relevant dimension tables
*/
insert into test.fact_gl
select gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, 
  coalesce(gtacct, 'none'), 
  gtctl_, gtdoc_, gtref_, gttamt, rrn
from test.ext_glptrns; 

-- -- NIGHTLY
drop table if exists test.ext_glptrns_tmp cascade;
CREATE TABLE test.ext_glptrns_tmp(
  gtco_ citext, gttrn_ bigint, gtseq_ integer,
  gtdtyp citext, gttype citext, gtpost citext,
  gtrsts citext,gtadjust citext,gtpsel citext,
  gtjrnl citext,gtdate date,gtrdate date,
  gtsdate date,gtacct citext,gtctl_ citext,
  gtdoc_ citext,gtrdoc_ citext,gtrdtyp citext,
  gtodoc_ citext,gtref_ citext,gtvnd_ citext,
  gtdesc citext,gttamt numeric(11,2),gtcost numeric(11,2),
  gtctlo citext,gtoco_ citext,rrn bigint unique);
create unique index on test.ext_glptrns_tmp(gttrn_,gtseq_) where gtpost = 'Y';

insert into test.ext_glptrns_tmp
select *
from test.ext_glptrns_0531;

insert into test.ext_glptrns
select * -- 13664 rows
from test.ext_glptrns_tmp a
where not exists (
  select 1
  from test.ext_glptrns
  where rrn = a.rrn);

insert into test.fact_gl
select gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, 
  coalesce(gtacct, 'none'), 
  gtctl_, gtdoc_, gtref_, gttamt, rrn
from test.ext_glptrns_tmp a 
where not exists (
  select 1
  from test.fact_gl
  where rrn = a.rrn);

select count(*) from test.fact_gl

