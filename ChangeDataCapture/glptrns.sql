﻿select max(gtdate) -- 01/01/2015
from test.ext_arkona_glptrns

select count(*) -- 404720
from test.ext_arkona_glptrns 

alter table test.ext_arkona_glptrns
alter column gttrn_ type bigint

alter table test.ext_arkona_glptrns
alter column gtseq_ type integer

alter table test.ext_arkona_glptrns
alter column gtco_ type citext,
alter column gtdype type citext,
alter column gttype type citext,
alter column gtpost type citext,
alter column gtrsts type citext,
alter column gtadjust type citext,
alter column gtpsel type citext,
alter column gtjrnl type citext,
alter column gtacct type citext,
alter column gtctl_ type citext,
alter column gtdoc_ type citext,
alter column gtrdoc_ type citext,
alter column gtrdtyp type citext,
alter column gtodoc_ type citext,
alter column gtref_ type citext,
alter column gtvnd_ type citext,
alter column gtdesc type citext,
alter column gtctlo type citext,
alter column gtoco_ type citext

!!!
in arkona, gllptrns is a file rather than a table, ie, created with rpg rather than sql
does not have a primary key 
!!!

-- this fails: (RY1, 2761914, 1) is duplicated.
ALTER TABLE test.ext_arkona_glptrns ADD PRIMARY KEY (gtco_, gttrn_, gtseq_);
-- aha void
select * from test.ext_arkona_glptrns where gttrn_ = 2761914

create unique index 
on test.ext_arkona_glptrns
using btree(gtco_, gttrn_, gtseq_)
where gtpost <> 'V'
  AND gttrn_ <> -555555555;
-- this failed

select * from test.ext_arkona_glptrns where gttrn_ = 2810557

select *
from test.ext_Arkona_glptrns
where gtpost = 'V'

select *
from test.ext_arkona_glptrns
where gttrn_ = 0


select * from test.ext_arkona_glptrns order by gttrn_ desc limit 1000

select *
from (
  select y.*, md5(y::text)
  from test.ext_Arkona_glptrns y) aa
where aa.md5 in (
  select md5
  from (
    select md5(ext_arkona_glptrns::text)
    from test.ext_arkona_glptrns
    where gtpost <> 'v') a 
  group by md5 having count(*) > 1)


select md5('abc')
union
select md5('ABC')

select (ext_arkona_glptrns::text)
from test.ext_arkona_glptrns
limit 1000

-- 11/20
enuf fucking around
ext -> stg with PK

CREATE TABLE test.stg_arkona_glptrns
(
  gtco_ citext, -- GTCO# : Company Number
  gttrn_ bigint, -- GTTRN# : Trans Number
  gtseq_ integer, -- GTSEQ# : Tran Seq Number
  gtdtyp citext, -- GTDTYP : Doc Type
  gttype citext, -- GTTYPE : Record Type
  gtpost citext, -- GTPOST : Post Status
  gtrsts citext, -- GTRSTS : Reconcile Status
  gtadjust citext, -- GTADJUST : Adjustment Tran
  gtpsel citext, -- GTPSEL : Select to Pay
  gtjrnl citext, -- GTJRNL : Journal
  gtdate date, -- GTDATE : Transaction Date
  gtrdate date, -- GTRDATE : Reconciled Date
  gtsdate date, -- GTSDATE : A/R Statemnt Date
  gtacct citext, -- GTACCT : Account Number
  gtctl_ citext, -- GTCTL# : Control Number
  gtdoc_ citext, -- GTDOC# : Document Number
  gtrdoc_ citext, -- GTRDOC# : Reconciled Doc#
  gtrdtyp citext, -- GTRDTYP : Recon Doc Type
  gtodoc_ citext, -- GTODOC# : Outside Doc#
  gtref_ citext, -- GTREF# : Reference Number
  gtvnd_ citext, -- GTVND# : Vendor Number
  gtdesc citext, -- GTDESC : Description
  gttamt numeric(11,2), -- GTTAMT : Transaction Amt
  gtcost numeric(11,2), -- GTCOST : Transaction Cost
  gtctlo citext, -- GTCTLO : Control Overide
  gtoco_ citext, -- GTOCO# : Originating Co#

-- code_title PRIMARY KEY(code,title)  
  constraint stg_arkona_glptrns_pk primary key (gtco_, gttrn_, gtseq_)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE test.stg_arkona_glptrns
  OWNER TO postgres;
COMMENT ON COLUMN test.stg_arkona_glptrns.gtco_ IS 'GTCO# : Company Number';
COMMENT ON COLUMN test.stg_arkona_glptrns.gttrn_ IS 'GTTRN# : Trans Number';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtseq_ IS 'GTSEQ# : Tran Seq Number';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtdtyp IS 'GTDTYP : Doc Type';
COMMENT ON COLUMN test.stg_arkona_glptrns.gttype IS 'GTTYPE : Record Type';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtpost IS 'GTPOST : Post Status';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtrsts IS 'GTRSTS : Reconcile Status';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtadjust IS 'GTADJUST : Adjustment Tran';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtpsel IS 'GTPSEL : Select to Pay';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtjrnl IS 'GTJRNL : Journal';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtdate IS 'GTDATE : Transaction Date';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtrdate IS 'GTRDATE : Reconciled Date';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtsdate IS 'GTSDATE : A/R Statemnt Date';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtacct IS 'GTACCT : Account Number';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtctl_ IS 'GTCTL# : Control Number';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtdoc_ IS 'GTDOC# : Document Number';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtrdoc_ IS 'GTRDOC# : Reconciled Doc#';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtrdtyp IS 'GTRDTYP : Recon Doc Type';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtodoc_ IS 'GTODOC# : Outside Doc#';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtref_ IS 'GTREF# : Reference Number';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtvnd_ IS 'GTVND# : Vendor Number';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtdesc IS 'GTDESC : Description';
COMMENT ON COLUMN test.stg_arkona_glptrns.gttamt IS 'GTTAMT : Transaction Amt';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtcost IS 'GTCOST : Transaction Cost';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtctlo IS 'GTCTLO : Control Overide';
COMMENT ON COLUMN test.stg_arkona_glptrns.gtoco_ IS 'GTOCO# : Originating Co#';

insert into test.stg_arkona_glptrns
select a.*
from test.ext_arkona_glptrns a
left join (
  select gtco_, gttrn_, gtseq_
  from test.ext_arkona_glptrns
  group by gtco_,gttrn_, gtseq_
  having count(*) > 1) b on a.gtco_ = b.gtco_
    and a.gttrn_ = b.gttrn_
    and a.gtseq_ = b.gtseq_
where a.gtpost <> 'V'
  AND a.gttrn_ > 0
  and b.gtco_ is null 


drop table  test.ext_arkona_glptrns_tmp_1;
CREATE TABLE test.ext_arkona_glptrns_tmp_1
(
  gtco_ citext, -- GTCO# : Company Number
  gttrn_ bigint, -- GTTRN# : Trans Number
  gtseq_ integer, -- GTSEQ# : Tran Seq Number
  gtdtyp citext, -- GTDTYP : Doc Type
  gttype citext, -- GTTYPE : Record Type
  gtpost citext, -- GTPOST : Post Status
  gtrsts citext, -- GTRSTS : Reconcile Status
  gtadjust citext, -- GTADJUST : Adjustment Tran
  gtpsel citext, -- GTPSEL : Select to Pay
  gtjrnl citext, -- GTJRNL : Journal
  gtdate date, -- GTDATE : Transaction Date
  gtrdate date, -- GTRDATE : Reconciled Date
  gtsdate date, -- GTSDATE : A/R Statemnt Date
  gtacct citext, -- GTACCT : Account Number
  gtctl_ citext, -- GTCTL# : Control Number
  gtdoc_ citext, -- GTDOC# : Document Number
  gtrdoc_ citext, -- GTRDOC# : Reconciled Doc#
  gtrdtyp citext, -- GTRDTYP : Recon Doc Type
  gtodoc_ citext, -- GTODOC# : Outside Doc#
  gtref_ citext, -- GTREF# : Reference Number
  gtvnd_ citext, -- GTVND# : Vendor Number
  gtdesc citext, -- GTDESC : Description
  gttamt numeric(11,2), -- GTTAMT : Transaction Amt
  gtcost numeric(11,2), -- GTCOST : Transaction Cost
  gtctlo citext, -- GTCTLO : Control Overide
  gtoco_ citext -- GTOCO# : Originating Co#
)
WITH (
  OIDS=FALSE
);
ALTER TABLE test.ext_arkona_glptrns_tmp_1
  OWNER TO postgres;
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtco_ IS 'GTCO# : Company Number';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gttrn_ IS 'GTTRN# : Trans Number';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtseq_ IS 'GTSEQ# : Tran Seq Number';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtdtyp IS 'GTDTYP : Doc Type';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gttype IS 'GTTYPE : Record Type';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtpost IS 'GTPOST : Post Status';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtrsts IS 'GTRSTS : Reconcile Status';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtadjust IS 'GTADJUST : Adjustment Tran';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtpsel IS 'GTPSEL : Select to Pay';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtjrnl IS 'GTJRNL : Journal';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtdate IS 'GTDATE : Transaction Date';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtrdate IS 'GTRDATE : Reconciled Date';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtsdate IS 'GTSDATE : A/R Statemnt Date';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtacct IS 'GTACCT : Account Number';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtctl_ IS 'GTCTL# : Control Number';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtdoc_ IS 'GTDOC# : Document Number';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtrdoc_ IS 'GTRDOC# : Reconciled Doc#';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtrdtyp IS 'GTRDTYP : Recon Doc Type';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtodoc_ IS 'GTODOC# : Outside Doc#';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtref_ IS 'GTREF# : Reference Number';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtvnd_ IS 'GTVND# : Vendor Number';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtdesc IS 'GTDESC : Description';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gttamt IS 'GTTAMT : Transaction Amt';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtcost IS 'GTCOST : Transaction Cost';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtctlo IS 'GTCTLO : Control Overide';
COMMENT ON COLUMN test.ext_arkona_glptrns_tmp_1.gtoco_ IS 'GTOCO# : Originating Co#';


